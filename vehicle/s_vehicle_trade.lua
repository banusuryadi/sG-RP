function setVehicleOwner( vehicle, ownerType, ownerId )
	if (isElement(vehicle)) and (tonumber(ownerType)) and (tonumber(ownerId)) then
		setElementData( vehicle, "vehicle.ownerType", ownerType )
		setElementData( vehicle, "vehicle.ownerId", ownerId )
		return true
	end
	return false
end

function setVehicleOnSale( vehicle, status )
	if (isElement(vehicle)) and (tonumber(status)) then
		setElementData( vehicle, "vehicle.onSale", status )
		return true
	end
	return false
end

function setVehiclePrice( vehicle, price )
	if (isElement(vehicle)) and (tonumber(price)) then
		setElementData( vehicle, "vehicle.price", price )
		return true
	end
	return false
end


addCommandHandler( {"sellvehicle", "sellveh"}, function ( commander, cmd, price )
	if (price) then
		local vehicle = getPedOccupiedVehicle( commander )
		if (isElement( vehicle )) then
			local ownerType = getElementData(vehicle, "vehicle.ownerType")
			local ownerId = getElementData(vehicle, "vehicle.ownerId")
			local playerId = getElementData(commander, "character.id")
			if (tonumber(ownerType) == 0) then
				if (tonumber(playerId) == tonumber(ownerId)) then
					setElementData(vehicle, "vehicle.onSale", 1)
					setElementData(vehicle, "vehicle.price", tonumber(price))
					outputChatBox( "Berhasil mengubah status penjualan kendaraan dengan harga $ "..price )
				end
			elseif(tonumber(ownerType) == 1) then
				local leader = exports.faction:getLeaderFaction(ownerId)
				if(leader) then
					if (tonumber(leader.owner_id) == tonumber(playerId)) then
						setElementData(vehicle, "vehicle.onSale", 1)
						setElementData(vehicle, "vehicle.price", tonumber(price))
						outputChatBox( "Berhasil mengubah status penjualan kendaraan dengan harga $ "..price )
					end
				end
			end
		end
	else
		outputChatBox( "/"..cmd.." [harga]", player, 3,169,244 )
		outputChatBox( "Harus berada di dalam kendaraan.", player, 3,169,244 )
	end
end )

addCommandHandler( {"unsellvehicle", "unsellveh"}, function ( commander, cmd )
	local vehicle = getPedOccupiedVehicle( commander )
	if (isElement( vehicle )) then
		local ownerType = getElementData(vehicle, "vehicle.ownerType")
		local ownerId = getElementData(vehicle, "vehicle.ownerId")
		local playerId = getElementData(commander, "character.id")
		if (tonumber(ownerType) == 0) then
			if (tonumber(playerId) == tonumber(ownerId)) then
				setElementData(vehicle, "vehicle.onSale", 0)
				outputChatBox( "Berhasil membatalkan penjualan kendaraan." )
			end
		elseif(tonumber(ownerType) == 1) then
			local leader = exports.faction:getLeaderFaction(ownerId)
			if(leader) then
				if (tonumber(leader.owner_id) == tonumber(playerId)) then
					setElementData(vehicle, "vehicle.onSale", 0)
					outputChatBox( "Berhasil membatalkan penjualan kendaraan." )
				end
			end
		end
	end
end )
