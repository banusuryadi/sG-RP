local dmvTruckModels = {[403]=true}
local windowless = {[568]=true, [601]=true, [424]=true, [457]=true, [480]=true, [485]=true, [486]=true, [528]=true, [530]=true, [531]=true, [532]=true, [571]=true, [572]=true}
local roofless = {[568]=true, [500]=true, [439]=true, [424]=true, [457]=true, [480]=true, [485]=true, [486]=true, [530]=true, [531]=true, [533]=true, [536]=true, [555]=true, [571]=true, [572]=true, [575]=true, [536]=true, [567]=true, [555]=true}
local enginelessVehicle = {[510]=true, [509]=true, [481]=true}
local lightlessVehicle = {[592]=true, [577]=true, [511]=true, [548]=true, [512]=true, [593]=true, [425]=true, [520]=true, [417]=true, [487]=true, [553]=true, [488]=true, [497]=true, [563]=true, [476]=true, [447]=true, [519]=true, [460]=true, [469]=true, [513]=true, [472]=true, [473]=true, [493]=true, [595]=true, [484]=true, [430]=true, [453]=true, [452]=true, [446]=true, [454]=true}
local locklessVehicle = {[581]=true, [509]=true, [481]=true, [462]=true, [521]=true, [463]=true, [510]=true, [522]=true, [461]=true, [448]=true, [468]=true, [586]=true}
local armoredCars = {[427]=true, [528]=true, [432]=true, [601]=true, [428]=true, [597]=true}
local platelessVehicles = {[592]=true, [553]=true, [577]=true, [488]=true, [511]=true, [497]=true, [548]=true, [563]=true, [512]=true, [476]=true, [593]=true, [447]=true, [425]=true, [519]=true, [520]=true, [460]=true, [417]=true, [469]=true, [487]=true, [513]=true, [509]=true, [481]=true, [510]=true, [472]=true, [473]=true, [493]=true, [595]=true, [484]=true, [430]=true, [453]=true, [452]=true, [446]=true, [454]=true, [571]=true}
local bike = {[581]=true, [509]=true, [481]=true, [462]=true, [521]=true, [463]=true, [510]=true, [522]=true, [461]=true, [448]=true, [468]=true, [586]=true}

local DB = exports.database
local INVENTORY = exports.inventory

-- local function setVehicleStats(vehicle)
-- 	setVehicleDamageProof( vehicle theVehicle, bool damageProof )
-- 	setVehicleColor( vehicle theVehicle, int r1, int g1, int b1, [int r2, int g2, int b2,] [int r3, int g3, int b3,] [int r4, int g4, int b4] )
-- end

function setVehicleColors( vehicle, color1, color2 )
	local r1, g1, b1 = fromJSON(color1)
	local r2, g2, b2 = fromJSON(color2)
	setVehicleColor( vehicle, r1, g1, b1, r2, g2, b2 )
end

function addNewVehicle(ownerType, ownerId, onSale, model, spawnX, spawnY, spawnZ, rotX, rotY, rotZ, interior, dimension, numberPlate, price)
	if (ownerType and ownerId and model and spawnX and spawnY and spawnZ and rotX and rotY and rotZ and interior and dimension and numberPlate) then
		local result, _, vehicle_id = DB:query( "INSERT INTO vehicles (`owner_id`, `owner_type`, `on_sale`, `model_id`, `health`, `last_x`, `last_y`, `last_z`, `rot_x`, `rot_y`, `rot_z`, `last_dimension`, `last_interior`, `date_created`, `lock_state`, `light_state`, `door_state`, `number_plate`,`engine_state`, `price`,`is_deleted`) VALUES ('??', '??', '??', '??', '??', '??', '??', '??', '??', '??', '??', '??', '??', '??', '??', '??', '??', '??','??','??','??')", ownerId, ownerType, onSale, model, 100, spawnX, spawnY, spawnZ, rotX, rotY, rotZ, dimension, interior, getRealTime().timestamp, 1, 0, toJSON({0,0,0,0,0,0}), numberPlate, 0, price, 0 )
		if (result) then
			addVehicle(vehicle_id, ownerType, ownerId, onSale, model, spawnX, spawnY, spawnZ, rotX, rotY, rotZ, interior, dimension, numberPlate, price, '[ [ 0, 0, 0 ] ]', '[ [ 0 ] ]')
			outputServerLog( "VEHICLE: Berhasil membuat 1 kendaraan ke dalam database, ID: "..vehicle_id )
		end
	end
end

function addVehicle(id, ownerType, ownerId, onSale, model, spawnX, spawnY, spawnZ, rotX, rotY, rotZ, interior, dimension, numberPlate, price, color1, color2)
	local vehicle
	vehicle = createVehicle( model, spawnX, spawnY, spawnZ, rotX, rotY, rotZ, numberPlate )
	setVehicleEngineState( vehicle, false )
	setElementInterior( vehicle, interior )
	setElementDimension( vehicle, dimension )

	setElementData( vehicle, "vehicle.id", id )
	setElementData( vehicle, "vehicle.ownerType", ownerType )
	setElementData( vehicle, "vehicle.ownerId", ownerId )
	setElementData( vehicle, "vehicle.onSale", onSale )
	setElementData( vehicle, "vehicle.price", price )
	setElementData( vehicle, "vehicle.engine", false )
	setVehicleColors( vehicle, color1, color2 )
	if(not isElement( vehicle )) then
		outputServerLog( "VEHICLE: Spawn kendaraan gagal. ID: "..id )
	end
	-- local x, y, z = getElementPosition(vehicle)
end

function saveVehicle(vehicle)
	if (not isElement( vehicle )) then return end

	local id = getElementData( vehicle, "vehicle.id" )
	local dimension = getElementDimension( vehicle )
	local interior = getElementInterior( vehicle )
	local ownerType = getElementData( vehicle, "vehicle.ownerType")
	local ownerId = getElementData( vehicle, "vehicle.ownerId")
	local onSale = getElementData( vehicle, "vehicle.onSale")
	local price = getElementData( vehicle, "vehicle.price")
	local health = getElementHealth( vehicle )
	local numberPlate = getVehiclePlateText( vehicle )
	local x, y, z = getElementPosition( vehicle )
	local rot_x, rot_y, rot_z = getElementRotation( vehicle )
	local r1, g1, b1, r2, g2, b2 = getVehicleColor(vehicle)
	local color1 = toJSON( {r1, g1, b1} )
	local color2 = toJSON( {r2, g2, b2} )

	local query = DB:execute( "UPDATE vehicles SET `owner_id` = '??', `owner_type` = '??', `on_sale` = '??', `health` = '??', last_x = '??', last_y = '??', last_z = '??', rot_x = '??', rot_y = '??', rot_z = '??', `number_plate` = '??', `color1` = '??', `color2` = '??', `last_interior` = '??', `last_dimension` = '??', `price` = '??' WHERE id = '??'", ownerId, ownerType, onSale, health, x, y, z, rot_x, rot_y, rot_z, numberPlate, color1, color2, interior, dimension, price, id )
	if (query) then
		outputServerLog( "VEHICLE: Berhasil mengupdate kendaraan ke dalam database. ID: "..id )
		return true
	end
end

function loadVehicles(vehicle)
	local result = DB:query( "SELECT * FROM vehicles" )
	if (result) then
		for _,row in pairs(result) do
			if (row["is_deleted"] == 0) then
				addVehicle(row["id"], row["owner_type"], row["owner_id"], row["on_sale"], row["model_id"], row["last_x"], row["last_y"], row["last_z"], row["rot_x"], row["rot_y"], row["rot_z"], row["last_interior"], row["last_dimension"], row["number_plate"], row["price"], row["color1"], row["color2"] )
			end
		end
	end
end

function toggleVehicleEngine( vehicle )
	local vehicleId = getElementData( vehicle, "vehicle.id" )
	local vehicleModel = getElementModel( vehicle )
	if (enginelessVehicle[vehicleModel] or bike[vehicleModel]) then return end
	if(INVENTORY:hasItem(client, 1, tostring(vehicleId))) then
		setElementData( vehicle, "vehicle.engine", (not getVehicleEngineState( vehicle )) )
		setVehicleEngineState( vehicle, (not getVehicleEngineState( vehicle )) )
		-- in future remove the key while engine starting, give driver the key otherwise.
		outputChatBox( "Vehicle engine " .. (getVehicleEngineState( vehicle ) == true and "Started." or "Stopped."), client)
	end
end

function toggleVehicleLocked( vehicle )
	local vehicleId = getElementData( vehicle, "vehicle.id" )
	local vehicleModel = getElementModel( vehicle )
	if (locklessVehicle[vehicleModel] or bike[vehicleModel]) then return end
	if(INVENTORY:hasItem(client, 1, tostring(vehicleId))) then
		setVehicleLocked( vehicle, (not isVehicleLocked( vehicle )) )
		outputChatBox( "Vehicle " .. (isVehicleLocked( vehicle ) == true and "Locked." or "Unlocked."), client)
	end
end

addEvent( "onClientToggleEngine", true )
addEventHandler( "onClientToggleEngine", root, function ( vehicle )
	if (source ~= client) then return end
	toggleVehicleEngine(vehicle)
end )

addEvent( "onClientToggleLock", true )
addEventHandler( "onClientToggleLock", root, function ( vehicle )
	if (source ~= client) then return end
	toggleVehicleLocked(vehicle)
end )

addEventHandler( "onVehicleEnter", root, function ( player, seat, jacked, door )
	setVehicleEngineState( source, getElementData( source, "vehicle.engine" ) )
end )

addEventHandler( "onVehicleStartEnter", root, function ( player, seat, jacked, door )
	if(isElement( jacked )) then
		cancelEvent(  )
	end
end )

addEventHandler( "onResourceStop", resourceRoot, function ( )
	for i,v in ipairs(getElementsByType("vehicle")) do
		if (getElementData( v, "vehicle.id" )) and (getElementData( v, "vehicle.ownerType")) then
			saveVehicle(v)
		end
	end
end )

addEventHandler( "onResourceStart", resourceRoot, function ( )
	loadVehicles()
end )

-- commands

local addCommandHandler_ = addCommandHandler
	addCommandHandler = function(commandName, fn, restricted, caseSensitive)
	if (type(commandName) ~= "table") then
		commandName = {commandName}
	end
	for key, value in ipairs(commandName) do
		if (key == 1) then
			addCommandHandler_(value, fn, restricted, false)
		else
			addCommandHandler_(value,
				function(player, ...)
					fn(player, ...)
				end, false, false
			)
		end
	end
end

addCommandHandler( {"createvehicle", "createveh", "cv", "makevehicle", "makeveh", "addveh", "addvehicle"}, 
	function ( player, commandName, ownerType, owner, model, numberPlate, onSale, price )
		local admin_level = getElementData( player, "user.adminLevel" )
		if (admin_level) then
			if (admin_level > 4) then
				if (ownerType and owner and model and numberPlate and onSale and price) then
					local x, y, z = exports.character:getPositionFromElementOffset( player, 0, 4, 0 )
					local rotX, rotY, rotZ = getElementRotation( player )
					
					addNewVehicle(ownerType, owner, onSale, model, x, y, z, rotX, rotY, rotZ, getElementInterior( player ), getElementDimension( player ), numberPlate, price)
				else
					outputChatBox( "/"..commandName.." [ownerType] [ownerId] [vehicle model] [plat nomor] [dijual] [harga]", player, 3,169,244 )
					outputChatBox( "[ownerType] 0. Player, 1. Faction", player, 3,169,244 )
				end
			end
		end
	end 
)

addCommandHandler( {"gotoveh"}, function ( player, commandName, vehId )
	local admin_level = getElementData( player, "user.adminLevel" )
	if (admin_level) then
		if (admin_level > 4) then
			local x, y, z = getVehiclePosition(tonumber(vehId))
			setElementPosition( player, x, y, z, true )
		end
	end
end )