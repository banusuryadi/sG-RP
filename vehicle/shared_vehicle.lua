function getVehiclePosition( vehId )
	local vehicle = getVehicleById( vehId )
	if (isElement( vehicle )) then
		local x, y, z = getElementPosition( vehicle )
		return x, y, z
	end
end

function getVehicleId( vehicle )
	return ( isElement( vehicle ) and getElementData( vehicle, "vehicle.id" ) ) and tonumber( getElementData( vehicle, "vehicle.id" ) ) or false
end

function getVehicleById( vehId )
	for _, vehicle in ipairs( getElementsByType( "vehicle" ) ) do
		if ( getVehicleId( vehicle ) == vehId ) then
			return vehicle
		end
	end	
	return false
end