local hitVehicle
addEventHandler( "onClientColShapeHit", root, function ( hitElement, matchingDimension )
	if(getElementAttachedTo( source ) == localPlayer) then
		if (getElementType( hitElement ) == "vehicle") then
			hitVehicle = hitElement
		end
	end
end )

addEventHandler( "onClientColShapeLeave", root, function ( hitElement, matchingDimension )
	if(getElementAttachedTo( source ) == localPlayer) then
		if (getElementType( hitElement ) == "vehicle") then
			hitVehicle = nil
		end
	end
end )

addEventHandler( "onClientPlayerRadioSwitch", localPlayer, function (  )
	setRadioChannel( 0 )
	cancelEvent(  )
end )

addEventHandler( "onClientResourceStart", resourceRoot, function (  )
	setRadioChannel( 0 )
	bindKey( "k", "up", function (  )
		if (isElement( hitVehicle )) then
			triggerServerEvent( "onClientToggleLock", localPlayer, hitVehicle )
		end
	end)
	bindKey( "j", "up", function (  )
		local vehicle = getPedOccupiedVehicle( localPlayer )
		local seat = getPedOccupiedVehicleSeat( localPlayer )
		if (isElement( vehicle )) and (seat == 0) then
			triggerServerEvent( "onClientToggleEngine", localPlayer, vehicle )
		end
	end)
end )