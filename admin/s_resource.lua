addCommandHandler("restartres",
	function(player, cmd, resource)
		-- if (not exports['roleplay-accounts']:isClientHeadAdmin(player)) then
		-- 	outputServerLog("Command Error: " .. getPlayerName(player) .. " tried to execute command /" .. cmd .. ".")
		-- 	return
		-- else
			if (not resource) then
				outputChatBox("SYNTAX: /" .. cmd .. " [resource name]", player, 210, 160, 25, false)
				return
			else
				local res = getResourceFromName(resource)
				
				if (not res) then
					outputChatBox("Couldn't find a resource with that name.", player, 245, 20, 20, false)
					return
				end
				
				if (getResourceState(res) == "running") then
					restartResource(res)
				else
					outputChatBox("The resource has to be running in order to restart it.", player, 245, 20, 20, false)
				end
			end
		-- end
	end
)

addCommandHandler("stopres",
	function(player, cmd, resource)
		-- if (not exports['roleplay-accounts']:isClientHeadAdmin(player)) then
		-- 	outputServerLog("Command Error: " .. getPlayerName(player) .. " tried to execute command /" .. cmd .. ".")
		-- 	return
		-- else
			if (not resource) then
				outputChatBox("SYNTAX: /" .. cmd .. " [resource name]", player, 210, 160, 25, false)
				return
			else
				local res = getResourceFromName(resource)
				
				if (not res) then
					outputChatBox("Couldn't find a resource with that name.", player, 245, 20, 20, false)
					return
				end
				
				if (getResourceState(res) == "running") then
					stopResource(res)
				else
					outputChatBox("The resource has to be running in order to stop it.", player, 245, 20, 20, false)
				end
			end
		-- end
	end
)

addCommandHandler("startres",
	function(player, cmd, resource)
		-- if (not exports['roleplay-accounts']:isClientHeadAdmin(player)) then
		-- 	outputServerLog("Command Error: " .. getPlayerName(player) .. " tried to execute command /" .. cmd .. ".")
		-- 	return
		-- else
			if (not resource) then
				outputChatBox("SYNTAX: /" .. cmd .. " [resource name]", player, 210, 160, 25, false)
				return
			else
				local res = getResourceFromName(resource)
				
				if (not res) then
					outputChatBox("Couldn't find a resource with that name.", player, 245, 20, 20, false)
					return
				end
				
				if (getResourceState(res) == "loaded") then
					startResource(res)
				else
					outputChatBox("The resource has to be stopped and loaded in order to started it.", player, 245, 20, 20, false)
				end
			end
		-- end
	end
)
