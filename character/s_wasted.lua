addEventHandler( "onPlayerWasted", root, function ( ammo, killer, weapon, bodypart, stealth )
	fadeCamera(source, false, 7.5)
	
	local model = getElementModel( source )
	local x, y, z = getElementPosition( source )
	local dist = getDistanceBetweenPoints3D(x, y, z, -2657.44, 696.25, 27.93)
	local totalCharge = 30
	
	if (dist >= 500) then
		totalCharge = math.floor(((dist-(dist/2))/2)/4)
	end

	-- take money from bank here
	
	setTimer( function ( player, model, dist, totalCharge )
		if (isElement( player )) then
			spawnPlayer( player, -2657.44, 696.25, 27.93, 0, model)
			fadeCamera(player, true, 2.5)
			setCameraTarget( player, player )
			setTimer(setElementFrozen, 500, 1, player, false)
		end
	end, 7.5+dist*math.random(6, 10), 1, source, model, dist, totalCharge )
end )