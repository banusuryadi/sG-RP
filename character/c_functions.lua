local origins = {
	[1] 	=	{"AF",	"Afghanistan"},
	[2] 	=	{"AL",	"Albania"},
	[3] 	=	{"DZ",	"Algeria"},
	[4] 	=	{"DS",	"American Samoa"},
	[5] 	=	{"AD",	"Andorra"},
	[6] 	=	{"AO",	"Angola"},
	[7] 	=	{"AI",	"Anguilla"},
	[8] 	=	{"AQ",	"Antarctica"},
	[9] 	=	{"AG",	"Antigua and Barbuda"},
	[10] 	=	{"AR",	"Argentina"},
	[11] 	=	{"AM",	"Armenia"},
	[12] 	=	{"AW",	"Aruba"},
	[13] 	=	{"AU",	"Australia"},
	[14] 	=	{"AT",	"Austria"},
	[15] 	=	{"AZ",	"Azerbaijan"},
	[16] 	=	{"BS",	"Bahamas"},
	[17] 	=	{"BH",	"Bahrain"},
	[18] 	=	{"BD",	"Bangladesh"},
	[19] 	=	{"BB",	"Barbados"},
	[20] 	=	{"BY",	"Belarus"},
	[21] 	=	{"BE",	"Belgium"},
	[22] 	=	{"BZ",	"Belize"},
	[23] 	=	{"BJ",	"Benin"},
	[24] 	=	{"BM",	"Bermuda"},
	[25] 	=	{"BT",	"Bhutan"},
	[26] 	=	{"BO",	"Bolivia"},
	[27] 	=	{"BA",	"Bosnia and Herzegovina"},
	[28] 	=	{"BW",	"Botswana"},
	[29] 	=	{"BV",	"Bouvet Island"},
	[30] 	=	{"BR",	"Brazil"},
	[31] 	=	{"IO",	"British Indian Ocean Territory"},
	[32] 	=	{"BN",	"Brunei Darussalam"},
	[33] 	=	{"BG",	"Bulgaria"},
	[34] 	=	{"BF",	"Burkina Faso"},
	[35] 	=	{"BI",	"Burundi"},
	[36] 	=	{"KH",	"Cambodia"},
	[37] 	=	{"CM",	"Cameroon"},
	[38] 	=	{"CA",	"Canada"},
	[39] 	=	{"CV",	"Cape Verde"},
	[40] 	=	{"KY",	"Cayman Islands"},
	[41] 	=	{"CF",	"Central African Republic"},
	[42] 	=	{"TD",	"Chad"},
	[43] 	=	{"CL",	"Chile"},
	[44] 	=	{"CN",	"China"},
	[45] 	=	{"CX",	"Christmas Island"},
	[46] 	=	{"CC",	"Cocos (Keeling) Islands"},
	[47] 	=	{"CO",	"Colombia"},
	[48] 	=	{"KM",	"Comoros"},
	[49] 	=	{"CG",	"Congo"},
	[50] 	=	{"CK",	"Cook Islands"},
	[51] 	=	{"CR",	"Costa Rica"},
	[52] 	=	{"HR",	"Croatia (Hrvatska)"},
	[53] 	=	{"CU",	"Cuba"},
	[54] 	=	{"CY",	"Cyprus"},
	[55] 	=	{"CZ",	"Czech Republic"},
	[56] 	=	{"DK",	"Denmark"},
	[57] 	=	{"DJ",	"Djibouti"},
	[58] 	=	{"DM",	"Dominica"},
	[59] 	=	{"DO",	"Dominican Republic"},
	[60] 	=	{"TP",	"East Timor"},
	[61] 	=	{"EC",	"Ecuador"},
	[62] 	=	{"EG",	"Egypt"},
	[63] 	=	{"SV",	"El Salvador"},
	[64] 	=	{"GQ",	"Equatorial Guinea"},
	[65] 	=	{"ER",	"Eritrea"},
	[66] 	=	{"EE",	"Estonia"},
	[67] 	=	{"ET",	"Ethiopia"},
	[68] 	=	{"FK",	"Falkland Islands (Malvinas)"},
	[69] 	=	{"FO",	"Faroe Islands"},
	[70] 	=	{"FJ",	"Fiji"},
	[71] 	=	{"FI",	"Finland"},
	[72] 	=	{"FR",	"France"},
	[73] 	=	{"FX",	"France, Metropolitan"},
	[74] 	=	{"GF",	"French Guiana"},
	[75] 	=	{"PF",	"French Polynesia"},
	[76] 	=	{"TF",	"French Southern Territories"},
	[77] 	=	{"GA",	"Gabon"},
	[78] 	=	{"GM",	"Gambia"},
	[79] 	=	{"GE",	"Georgia"},
	[80] 	=	{"DE",	"Germany"},
	[81] 	=	{"GH",	"Ghana"},
	[82] 	=	{"GI",	"Gibraltar"},
	[83] 	=	{"GK",	"Guernsey"},
	[84] 	=	{"GR",	"Greece"},
	[85] 	=	{"GL",	"Greenland"},
	[86] 	=	{"GD",	"Grenada"},
	[87] 	=	{"GP",	"Guadeloupe"},
	[88] 	=	{"GU",	"Guam"},
	[89] 	=	{"GT",	"Guatemala"},
	[90] 	=	{"GN",	"Guinea"},
	[91] 	=	{"GW",	"Guinea-Bissau"},
	[92] 	=	{"GY",	"Guyana"},
	[93] 	=	{"HT",	"Haiti"},
	[94] 	=	{"HM",	"Heard and Mc Donald Islands"},
	[95] 	=	{"HN",	"Honduras"},
	[96] 	=	{"HK",	"Hong Kong"},
	[97] 	=	{"HU",	"Hungary"},
	[98] 	=	{"IS",	"Iceland"},
	[99] 	=	{"IN",	"India"},
	[100] 	=	{"IM",	"Isle of Man"},
	[101] 	=	{"ID",	"Indonesia"},
	[102] 	=	{"IR",	"Iran (Islamic Republic of)"},
	[103] 	=	{"IQ",	"Iraq"},
	[104] 	=	{"IE",	"Ireland"},
	[105] 	=	{"IL",	"Israel"},
	[106] 	=	{"IT",	"Italy"},
	[107] 	=	{"CI",	"Ivory Coast"},
	[108] 	=	{"JE",	"Jersey"},
	[109] 	=	{"JM",	"Jamaica"},
	[110] 	=	{"JP",	"Japan"},
	[111] 	=	{"JO",	"Jordan"},
	[112] 	=	{"KZ",	"Kazakhstan"},
	[113] 	=	{"KE",	"Kenya"},
	[114] 	=	{"KI",	"Kiribati"},
	[115] 	=	{"KP",	"Korea, Democratic People's Republic of"},
	[116] 	=	{"KR",	"Korea, Republic of"},
	[117] 	=	{"XK",	"Kosovo"},
	[118] 	=	{"KW",	"Kuwait"},
	[119] 	=	{"KG",	"Kyrgyzstan"},
	[120] 	=	{"LA",	"Lao People's Democratic Republic"},
	[121] 	=	{"LV",	"Latvia"},
	[122] 	=	{"LB",	"Lebanon"},
	[123] 	=	{"LS",	"Lesotho"},
	[124] 	=	{"LR",	"Liberia"},
	[125] 	=	{"LY",	"Libyan Arab Jamahiriya"},
	[126] 	=	{"LI",	"Liechtenstein"},
	[127] 	=	{"LT",	"Lithuania"},
	[128] 	=	{"LU",	"Luxembourg"},
	[129] 	=	{"MO",	"Macau"},
	[130] 	=	{"MK",	"Macedonia"},
	[131] 	=	{"MG",	"Madagascar"},
	[132] 	=	{"MW",	"Malawi"},
	[133] 	=	{"MY",	"Malaysia"},
	[134] 	=	{"MV",	"Maldives"},
	[135] 	=	{"ML",	"Mali"},
	[136] 	=	{"MT",	"Malta"},
	[137] 	=	{"MH",	"Marshall Islands"},
	[138] 	=	{"MQ",	"Martinique"},
	[139] 	=	{"MR",	"Mauritania"},
	[140] 	=	{"MU",	"Mauritius"},
	[141] 	=	{"TY",	"Mayotte"},
	[142] 	=	{"MX",	"Mexico"},
	[143] 	=	{"FM",	"Micronesia, Federated States of"},
	[144] 	=	{"MD",	"Moldova, Republic of"},
	[145] 	=	{"MC",	"Monaco"},
	[146] 	=	{"MN",	"Mongolia"},
	[147] 	=	{"ME",	"Montenegro"},
	[148] 	=	{"MS",	"Montserrat"},
	[149] 	=	{"MA",	"Morocco"},
	[150] 	=	{"MZ",	"Mozambique"},
	[151] 	=	{"MM",	"Myanmar"},
	[152] 	=	{"NA",	"Namibia"},
	[153] 	=	{"NR",	"Nauru"},
	[154] 	=	{"NP",	"Nepal"},
	[155] 	=	{"NL",	"Netherlands"},
	[156] 	=	{"AN",	"Netherlands Antilles"},
	[157] 	=	{"NC",	"New Caledonia"},
	[158] 	=	{"NZ",	"New Zealand"},
	[159] 	=	{"NI",	"Nicaragua"},
	[160] 	=	{"NE",	"Niger"},
	[161] 	=	{"NG",	"Nigeria"},
	[162] 	=	{"NU",	"Niue"},
	[163] 	=	{"NF",	"Norfolk Island"},
	[164] 	=	{"MP",	"Northern Mariana Islands"},
	[165] 	=	{"NO",	"Norway"},
	[166] 	=	{"OM",	"Oman"},
	[167] 	=	{"PK",	"Pakistan"},
	[168] 	=	{"PW",	"Palau"},
	[169] 	=	{"PS",	"Palestine"},
	[170] 	=	{"PA",	"Panama"},
	[171] 	=	{"PG",	"Papua New Guinea"},
	[172] 	=	{"PY",	"Paraguay"},
	[173] 	=	{"PE",	"Peru"},
	[174] 	=	{"PH",	"Philippines"},
	[175] 	=	{"PN",	"Pitcairn"},
	[176] 	=	{"PL",	"Poland"},
	[177] 	=	{"PT",	"Portugal"},
	[178] 	=	{"PR",	"Puerto Rico"},
	[179] 	=	{"QA",	"Qatar"},
	[180] 	=	{"RE",	"Reunion"},
	[181] 	=	{"RO",	"Romania"},
	[182] 	=	{"RU",	"Russian Federation"},
	[183] 	=	{"RW",	"Rwanda"},
	[184] 	=	{"KN",	"Saint Kitts and Nevis"},
	[185] 	=	{"LC",	"Saint Lucia"},
	[186] 	=	{"VC",	"Saint Vincent and the Grenadines"},
	[187] 	=	{"WS",	"Samoa"},
	[188] 	=	{"SM",	"San Marino"},
	[189] 	=	{"ST",	"Sao Tome and Principe"},
	[190] 	=	{"SA",	"Saudi Arabia"},
	[191] 	=	{"SN",	"Senegal"},
	[192] 	=	{"RS",	"Serbia"},
	[193] 	=	{"SC",	"Seychelles"},
	[194] 	=	{"SL",	"Sierra Leone"},
	[195] 	=	{"SG",	"Singapore"},
	[196] 	=	{"SK",	"Slovakia"},
	[197] 	=	{"SI",	"Slovenia"},
	[198] 	=	{"SB",	"Solomon Islands"},
	[199] 	=	{"SO",	"Somalia"},
	[200] 	=	{"ZA",	"South Africa"},
	[201] 	=	{"GS",	"South Georgia South Sandwich Islands"},
	[202] 	=	{"ES",	"Spain"},
	[203] 	=	{"LK",	"Sri Lanka"},
	[204] 	=	{"SH",	"St. Helena"},
	[205] 	=	{"PM",	"St. Pierre and Miquelon"},
	[206] 	=	{"SD",	"Sudan"},
	[207] 	=	{"SR",	"Suriname"},
	[208] 	=	{"SJ",	"Svalbard and Jan Mayen Islands"},
	[209] 	=	{"SZ",	"Swaziland"},
	[210] 	=	{"SE",	"Sweden"},
	[211] 	=	{"CH",	"Switzerland"},
	[212] 	=	{"SY",	"Syrian Arab Republic"},
	[213] 	=	{"TW",	"Taiwan"},
	[214] 	=	{"TJ",	"Tajikistan"},
	[215] 	=	{"TZ",	"Tanzania, United Republic of"},
	[216] 	=	{"TH",	"Thailand"},
	[217] 	=	{"TG",	"Togo"},
	[218] 	=	{"TK",	"Tokelau"},
	[219] 	=	{"TO",	"Tonga"},
	[220] 	=	{"TT",	"Trinidad and Tobago"},
	[221] 	=	{"TN",	"Tunisia"},
	[222] 	=	{"TR",	"Turkey"},
	[223] 	=	{"TM",	"Turkmenistan"},
	[224] 	=	{"TC",	"Turks and Caicos Islands"},
	[225] 	=	{"TV",	"Tuvalu"},
	[226] 	=	{"UG",	"Uganda"},
	[227] 	=	{"UA",	"Ukraine"},
	[228] 	=	{"AE",	"United Arab Emirates"},
	[229] 	=	{"GB",	"United Kingdom"},
	[230] 	=	{"US",	"United States"},
	[231] 	=	{"UM",	"United States minor outlying islands"},
	[232] 	=	{"UY",	"Uruguay"},
	[233] 	=	{"UZ",	"Uzbekistan"},
	[234] 	=	{"VU",	"Vanuatu"},
	[235] 	=	{"VA",	"Vatican City State"},
	[236] 	=	{"VE",	"Venezuela"},
	[237] 	=	{"VN",	"Vietnam"},
	[238] 	=	{"VG",	"Virgin Islands (British)"},
	[239] 	=	{"VI",	"Virgin Islands (U.S.)"},
	[240] 	=	{"WF",	"Wallis and Futuna Islands"},
	[241] 	=	{"EH",	"Western Sahara"},
	[242] 	=	{"YE",	"Yemen"},
	[243] 	=	{"ZR",	"Zaire"},
	[244] 	=	{"ZM",	"Zambia"},
	[245] 	=	{"ZW",	"Zimbabwe"}
}

local texts = {
	["warnings"] = {
		["resolution"] = "WARNING: You are currently running on a resolution that we don't exclusively support. We suggest switching to 1024x768 resolution or higher for better support."
	},
	["notifications"] = {
		["motd"] = "MESSAGE OF THE DAY: "
	},
	["errors"] = {
		["error-submit"] = "ERROR: An error occured when trying to send the form. Please reconnect\nand retry.",
		["error-name"] = "ERROR: Nama terlalu pendek.",
		["error-namespace"] = "ERROR: Akhiran nama tidak bisa menggunakan spasi.",
		["error-namecapital"] = "ERROR: Awalan harus menggunakan huruf kapital.",
		["error-name-invchar"] = "ERROR: Hanya bisa menggunakan huruf A-Z. Pastikan juga nama pertama dipisah menggunakan spasi dengan nama kedua.",
		["error-name-restricted"] = "ERROR: The character name is restricted and cannot be used.",
		["error-name-inuse"] = "ERROR: The character name is already in use. Think of another\nname!",
		["error-desc"] = "ERROR: The description has to be at least 100 characters long\nand be less than 500 characters long."
	},
	["tips"] = {
		["username"] = "USERNAME\nA username has to be at least 3 characters long and be smaller than 20 characters. Some usernames are restricted. It cannot contain only numbers. It can only contain lowercase and uppercase letters as well as numbers and a few special characters such as _?!",
		["password"] = "PASSWORD\nA password has to be at least 6 characters long and be smaller than 30 characters. Simple passwords are restricted. It has to contain numbers and letters. It can only contain lowercase and uppercase letters, numbers and a few special characters such as _?!*",
		["username-illegal-characters"] = "SUBMIT FAILED\nApparently your username contains illegal characters. It cannot contain only numbers. It can only contain lowercase and uppercase letters as well as numbers and a few special characters such as _?!",
		["password-illegal-characters"] = "SUBMIT FAILED\nApparently your password contains illegal characters. It has to contain numbers and letters. It can only contain lowercase and uppercase letters, numbers and a few special characters such as _?!*",
		["username-restricted"] = "SUBMIT FAILED\nApparently your username is restricted and cannot be registered or logged in with.",
		["password-restricted"] = "SUBMIT FAILED\nApparently your password is restricted and cannot be registered or logged in with.",
		["username-length"] = "SUBMIT FAILED\nYour username is either too short or too long. Minimum amount of characters is 3, and maximum is 20.",
		["password-length"] = "SUBMIT FAILED\nYour password is either too short or too long. Minimum amount of characters is 6, and maximum is 30.",
		["empty"] = "SUBMIT FAILED\nApparently you haven't passed in enough information. Please fill in your login information to proceed.",
		["submit-invalid"] = "INVALID CREDIENTIALS\nThe username and/or password was wrong, or if you're registering, the username is already in use and you have to try to suit yourself a new username.",
		["register-success"] = "REGISTER SUCCESS\nA new account has been created with those details. Please log in in order to proceed in the process.",
		["already-logged-in"] = "LOGIN FAILED\nThis account is already logged in in-game. Log out from the account before logging in again."
	}
}
function getOrigins(  )
	return origins
end

function isValidCharacterName(string)
	local foundSpace, valid = false, true
	local lastChar, current = ' ', ''
	for i=1,#string do
		local char = string.sub(string, i, i)
		if (char == ' ') then
			if (i == #string) then
				valid = false
				return false, texts["errors"]["error-namespace"]
			else
				foundSpace = true
			end
			
			if (#current < 3	) then
				valid = false
				return false, texts["errors"]["error-name"]
			end
			current = ''
		elseif (lastChar == ' ') then
			if (char < 'A') or (char > 'Z') then
				valid = false
				return false, texts["errors"]["error-namecapital"]
			end
			current = current .. char
		elseif (char >= 'a' and char <= 'z') or (char >= 'A' and char <= 'Z') then
			current = current .. char
		else
			valid = false
			return false, texts["errors"]["error-name-invchar"]
		end
		lastChar = char
	end
	
	if (valid) and (foundSpace) and (#string < 25) and (#current >= 3	) then
		return true
	else
		return false, texts["errors"]["error-name"]
	end
end

function checkDate(str)

  -- perhaps some sanity checks to see if `str` really is a date

  local d, m, y = str:match("(%d+)/(%d+)/(%d+)")

  if (d and m and y) then
	  m, d, y = tonumber(m), tonumber(d), tonumber(y)

	  if d < 0 or d > 31 or m < 0 or m > 12 or y < 0 then
	    -- Cases that don't make sense
	    return false
	  elseif m == 4 or m == 6 or m == 9 or m == 11 then 
	    -- Apr, Jun, Sep, Nov can have at most 30 days
	    return d <= 30
	  elseif m == 2 then
	    -- Feb
	    if y%400 == 0 or (y%100 ~= 0 and y%4 == 0) then
	      -- if leap year, days can be at most 29
	      return d <= 29
	    else
	      -- else 28 days is the max
	      return d <= 28
	    end
	  else 
	    -- all other months can have at most 31 days
	    return d <= 31
	  end
  end
  return false
end

function getAge(str)
  	local day, month, year = str:match("(%d+)/(%d+)/(%d+)")

	local time = getRealTime();
	time.year = time.year + 1900;
	time.month = time.month + 1;
	
	year = time.year - year;
	month = time.month - month;
	
	if month < 0 then 
		year = year - 1 
	elseif month == 0 then
		if time.monthday < day then
			year = year - 1;
		end
	end
	
	return year;
end

local gWeekDays = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" }
function FormatDate(format, escaper, timestamp)
	Check("FormatDate", "string", format, "format", {"nil","string"}, escaper, "escaper", {"nil","string"}, timestamp, "timestamp")
	
	escaper = (escaper or "'"):sub(1, 1)
	local time = getRealTime(timestamp)
	local formattedDate = ""
	local escaped = false

	time.year = time.year + 1900
	time.month = time.month + 1
	
	local datetime = { d = ("%02d"):format(time.monthday), h = ("%02d"):format(time.hour), i = ("%02d"):format(time.minute), m = ("%02d"):format(time.month), s = ("%02d"):format(time.second), w = gWeekDays[time.weekday+1]:sub(1, 2), W = gWeekDays[time.weekday+1], y = tostring(time.year):sub(-2), Y = time.year }
	
	for char in format:gmatch(".") do
		if (char == escaper) then escaped = not escaped
		else formattedDate = formattedDate..(not escaped and datetime[char] or char) end
	end
	
	return formattedDate
end