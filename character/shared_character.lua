local DB = exports.database

function getCharacterId( player )
	return ( isElement( player ) and getElementData( player, "character.id" ) ) and tonumber( getElementData( player, "character.id" ) ) or false
end

function getPlayerByCharacterId( characterId )
	for _, player in ipairs( getElementsByType( "player" ) ) do
		if ( getCharacterId( player ) == characterId ) then
			return player
		end
	end	
	return false
end

function isCharacterPlayable( characterId )
	return DB:query_single("SELECT * FROM characters WHERE `id` = '??' AND `cked` = 0", characterId)
end