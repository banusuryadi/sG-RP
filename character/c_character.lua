local sw, sh = guiGetScreenSize(  )
local gw, gh = 1024, 768
local char = {
	window = {},
	edit = {},
	label = {},
	radio = {},
	combo = {},
	scrollpane = {},
	button = {}
}
local characters = {
	[1] = {},
	[2] = {},
	[3] = {},
	[4] = {},
	[5] = {}
}


local maleSkins = { 
	[1] = {1, 2, 23, 26, 27, 29, 30, 32, 33, 34, 35, 36, 37, 38, 43, 44, 45, 46, 47, 48, 50, 51, 52, 53, 58, 59, 60, 61, 62, 68, 70, 72, 73, 78, 81, 82, 94, 95, 96, 97, 98, 99, 100, 101, 108, 109, 110, 111, 112, 113, 114, 115, 116, 120, 121, 122, 124, 125, 126, 127, 128, 132, 133, 135, 137, 146, 147, 153, 154, 155, 158, 159, 160, 161, 162, 164, 165, 170, 171, 173, 174, 175, 177, 179, 181, 184, 186, 187, 188, 189, 200, 202, 204, 206, 209, 212, 213, 217, 223, 230, 234, 235, 236, 240, 241, 242, 247, 248, 250, 252, 254, 255, 258, 259, 261, 268, 272, 290, 291, 292, 295, 299, 303, 305, 306, 307, 308, 309, 312},
	[2] = {7, 14, 15, 16, 17, 18, 20, 21, 22, 24, 25, 28, 35, 36, 50, 51, 66, 67, 78, 79, 80, 83, 84, 102, 103, 104, 105, 106, 107, 134, 136, 142, 143, 144, 156, 163, 166, 168, 176, 180, 182, 183, 185, 220, 221, 222, 249, 253, 260, 262, 269, 270, 271, 293, 296, 297, 300, 301, 302, 310, 311},
	[3] = {49, 57, 58, 59, 60, 117, 118, 120, 121, 122, 123, 170, 186, 187, 203, 210, 227, 228, 229, 294}
}

local femaleSkins = {
	[1] = {12, 31, 38, 39, 40, 41, 53, 54, 55, 56, 64, 75, 77, 85, 87, 88, 89, 90, 91, 92, 93, 129, 130, 131, 138, 140, 145, 150, 151, 152, 157, 172, 178, 192, 193, 194, 196, 197, 198, 199, 201, 205, 211, 214, 216, 224, 225, 226, 231, 232, 233, 237, 243, 246, 251, 257, 263, 298},
	[2] = {9, 10, 11, 12, 13, 40, 41, 63, 64, 69, 76, 91, 139, 148, 190, 195, 207, 215, 218, 219, 238, 243, 244, 245, 256, 304},
	[3] = {38, 53, 54, 55, 56, 88, 141, 169, 178, 224, 225, 226, 263}
}

local gangSkins = {105, 106, 107, 102, 103, 104, 108, 109, 110, 114, 115, 116, 247, 248, 254, 111, 112, 113, 124, 125, 126, 127, 173, 174, 175, 117, 118, 120, 121, 122, 123}
local reserverSkins = {}
local DGS = exports.dgs

local selectedRace = 1
local selectedGender = maleSkins

local playerSelectedModel = nil

local function setTooltip( text, r, g, b, a )
	DGS:dgsSetText(char.label[6] or char.label[10], text)
	DGS:dgsLabelSetColor(char.label[6] or char.label[10], r, g, b, a)
end

local function processCreateCharacter(  )
	DGS:dgsSetEnabled(char.button[1], false)

	local characterName = DGS:dgsGetText(char.edit[1])
	local characterGender
	local characterRace = DGS:dgsComboBoxGetSelectedItem(char.combo[1])
	local characterBirthday = DGS:dgsGetText(char.edit[2])
	local characterBio = DGS:dgsGetText(char.edit[3])
	local characterOrigin = DGS:dgsComboBoxGetSelectedItem(char.combo[2])
	
	local checkName, msg = isValidCharacterName(characterName)
	if (not checkName) then
		setTooltip(msg, 255, 0, 0, 255)
		DGS:dgsSetEnabled(char.button[1], true)
		return
	end

	if (DGS:dgsRadioButtonGetSelected(char.radio[1])) then
		characterGender = 1
	else
		characterGender = 0
	end

	if (not checkDate(characterBirthday)) then
		setTooltip("ERROR: Tanggal lahir tidak sesuai format.", 255, 0, 0, 255)
		DGS:dgsSetEnabled(char.button[1], true)
		return
	end

	if (getAge(characterBirthday) < 18 or getAge(characterBirthday) > 60) then
		setTooltip("ERROR: Umur terlalu muda/tua.", 255, 0, 0, 255)
		DGS:dgsSetEnabled(char.button[1], true)
		return
	end
	

	if (#characterBio < 100 ) then
		setTooltip("ERROR: Biografi terlalu pendek.", 255, 0, 0, 255)
		DGS:dgsSetEnabled(char.button[1], true)
		return
	end

	if (playerSelectedModel == nil) then
		setTooltip("ERROR: Silahkan pilih penampilan.", 255, 0, 0, 255)
		DGS:dgsSetEnabled(char.button[1], true)
		return
	end
	
	setTooltip("Membuat karakter...", 0, 255, 0, 255)
	triggerServerEvent( "onClientSubmitNewCharacter", localPlayer, playerSelectedModel, characterName, characterGender, characterRace, characterBirthday, characterBio, characterOrigin)
end

local function displayCharacterCreation( hide )
	showCursor( true )
	char.window[2] = DGS:dgsCreateWindow(sw*0, sh*0.2, sw*1, sh*0.6, "Character Creation", false, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x96141414, 5, true)
	char.label[1] = DGS:dgsCreateLabel( 0.1, 0.05, 0.1, 0.1, "Nama", true, char.window[2] )
	char.edit[1] = DGS:dgsCreateEdit( 0.1, 0.1, 0.2, 0.08, "", true, char.window[2] )

	char.radio[1] = DGS:dgsCreateRadioButton( 0.1, 0.21, 0.01, 0.08, "Pria", true, char.window[2] )
	char.radio[2] = DGS:dgsCreateRadioButton( 0.2, 0.21, 0.01, 0.08, "Wanita", true, char.window[2] )

	char.label[2] = DGS:dgsCreateLabel( 0.1, 0.3, 0.1, 0.1, "Ras", true, char.window[2] )
	char.combo[1] = DGS:dgsCreateComboBox( 0.1, 0.35, 0.2, 0.08, true, char.window[2] )
	DGS:dgsComboBoxAddItem( char.combo[1], "Putih" )
	DGS:dgsComboBoxAddItem( char.combo[1], "Hitam" )
	DGS:dgsComboBoxAddItem( char.combo[1], "Asia" )
	DGS:dgsComboBoxSetSelectedItem( char.combo[1], 1 )

	char.label[3] = DGS:dgsCreateLabel( 0.1, 0.45, 0.1, 0.1, "Tanggal Lahir", true, char.window[2] )
	char.edit[2] = DGS:dgsCreateEdit( 0.1, 0.50, 0.2, 0.08, "", true, char.window[2] )
	char.label[8] = DGS:dgsCreateLabel( 0.2, 0.58, 0.1, 0.1, "(ex. 31/12/1995)", true, char.window[2] )
	
	char.label[9] = DGS:dgsCreateLabel( 0.1, 0.60, 0.1, 0.1, "Negara Asal", true, char.window[2] )
	char.combo[2] = DGS:dgsCreateComboBox( 0.1, 0.65, 0.2, 0.08, true, char.window[2] )
	
	for i,v in ipairs(getOrigins()) do
		DGS:dgsComboBoxAddItem( char.combo[2], v[2] )
	end
	DGS:dgsComboBoxSetSelectedItem( char.combo[2], 230 )

	char.label[4] = DGS:dgsCreateLabel( 0.1, 0.75, 0.1, 0.1, "Biografi", true, char.window[2] )
	char.edit[3] = DGS:dgsCreateEdit( 0.1, 0.80, 0.60, 0.08, "", true, char.window[2] )
	DGS:dgsEditSetMaxLength(char.edit[3], 254)

	char.label[5] = DGS:dgsCreateLabel( 0.2, 0.90, 0.1, 0.1, "(Min. 100 karakter)", true, char.window[2] )
	char.button[1] = DGS:dgsCreateButton(0.75, 0.80, 0.1, 0.1, "Simpan", true, char.window[2], 0xFFFFFFFF, 1, 1)
	char.label[6] = DGS:dgsCreateLabel( 0.75, 0.90, 0.1, 0.1, "", true, char.window[2] )
	char.label[7] = DGS:dgsCreateLabel( 0.35, 0.05, 0.1, 0.1, "Pilih Penampilan", true, char.window[2] )

	local function loadSkinsScrollPane( )
		if isElement( char.scrollpane[1] ) then
			destroyElement( char.scrollpane[1] )
		end
		char.scrollpane[1] =  DGS:dgsCreateScrollPane( 0.35, 0.1, 0.5, 0.65, true, char.window[2])
		local column = 0
		local rows = 0
		for i,v in ipairs(selectedGender[selectedRace]) do
			local sx = 0.05
			local sy = 0.05
			local skinsPath = dxCreateTexture( "images/skins/"..tostring(v)..".png" ) or _
			local skinButton = DGS:dgsCreateButton(sx+(0.2*(column)), sy+(0.4*(rows)), 0.25, 0.4, i, true, char.scrollpane[1], 0xFFFFFFFF, 1, 1, skinsPath, skinsPath, skinsPath, tocolor( 211, 211, 211,255 ), 0xFFFFFFFF, 0xFFFFFFFF)
			if (i%4==0) then
				column = 0
				rows = rows + 1
			else
				column = column + 1
			end
			addEventHandler( "onDgsMouseClick", skinButton, function ( butt, st )
				if (st == "up") then
					DGS:dgsSetVisible(char.scrollpane[1], false)
					char.button[2] = DGS:dgsCreateButton(0.35, 0.1, 0.125, 0.25, "Klik untuk ganti", true, char.window[2], 0xFFFFFFFF, 1, 1, skinsPath, skinsPath, skinsPath, tocolor( 255, 255, 255, 255 ), tocolor( 211, 211, 211, 255 ),tocolor( 255, 255, 255, 255 ))
					playerSelectedModel = v
					addEventHandler( "onDgsMouseClick", char.button[2], function ( b, state )
						if (state == "up") then
							playerSelectedModel = nil
							destroyElement( source )
							DGS:dgsSetVisible(char.scrollpane[1], true)
						end
					end)
				end
			end )
		end
	end
	loadSkinsScrollPane()

	addEventHandler( "onDgsMouseClick", char.button[1], function ( button, state )
		if (state == "up") then
			processCreateCharacter()
		end
	end )

	addEventHandler( "onDgsRadioButtonChange", char.radio[1], function ( )
		if (DGS:dgsRadioButtonGetSelected(char.radio[1])) then
			selectedGender = maleSkins
			loadSkinsScrollPane()
		else
			selectedGender = femaleSkins
			loadSkinsScrollPane()
		end
	end )
	DGS:dgsRadioButtonSetSelected( char.radio[1], true )

	addEventHandler( "onDgsComboBoxSelect", char.combo[1], function ( selected, prev )
		outputChatBox( selected )
		selectedRace = selected
		DGS:dgsComboBoxSetState(char.combo[1], false)
		loadSkinsScrollPane()
	end )

	addEventHandler( "onDgsComboBoxSelect", char.combo[2], function ( selected, prev )
		DGS:dgsComboBoxSetState(char.combo[2], false)
	end )
	
	addEventHandler( "onDgsFocus", char.edit[1], function (  )
		setTooltip("")
	end )	
	addEventHandler( "onDgsFocus", char.edit[3], function (  )
		setTooltip("")
	end )

end

local function displayCharacterSelection( hide )
	if ( hide == true ) then
		if (isElement( char.window[1] )) then
			destroyElement( char.window[1] )
		end
		showCursor( false )

		return
	end
	showCursor( true )

	char.window[1] = DGS:dgsCreateWindow(sw*0, sh*0.3, sw*1, sh*0.4, "Character Selection", false, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x96141414, 5, true)
	char.label[10] = DGS:dgsCreateLabel( 0.45, 0.90, 0.1, 0.1, "", true, char.window[1] )
	for i,v in ipairs(characters) do
		if (v["id"]) then
			local skinsPath = dxCreateTexture( "images/skins/"..v["skin"]..".png" ) or _
			local charButton = DGS:dgsCreateButton( (0.17*i)-0.08, 0.20, 0.12, 0.36, "", true, char.window[1], 0xFFFFFFFF, 1, 1, skinsPath, skinsPath, skinsPath, tocolor( 211, 211, 211,255 ), 0xFFFFFFFF, 0xFFFFFFFF )
			local nameLabel = DGS:dgsCreateLabel((0.17*i)-0.06, 0.08, 0.12, 0.1, v["name"], true, char.window[1])
			if (v["cked"] == 0) then
				local ckedLabel = DGS:dgsCreateLabel((0.17*i)-0.06, 0.60, 0.12, 0.1, "Alive", true, char.window[1], tocolor( 0, 255, 0, 255))
			else
				DGS:dgsSetEnabled(charButton, false)
				local ckedLabel = DGS:dgsCreateLabel((0.17*i)-0.06, 0.60, 0.12, 0.1, "Deceased", true, char.window[1], tocolor( 255, 0, 0, 255))
			end
			if (v["last_login"]) then
				local lastLogins = getRealTime( v["last_login"] )
				local day = lastLogins.monthday
				local month = lastLogins.month
				local year = lastLogins.year
				local minute = lastLogins.minute
				local hour = lastLogins.hour
				local formattedLastLogin = "Last login:\n"..day.."/"..month.."/"..year.." "..hour..":"..minute
				local lastLoginLabel = DGS:dgsCreateLabel((0.17*i)-0.06, 0.70, 0.12, 0.1, formattedLastLogin, true, char.window[1])
			end
			addEventHandler ( "onDgsMouseClick", charButton, function ( button, state )
				if (state == "up") then
					triggerServerEvent( "onCharacterSelect", localPlayer, v["id"] )
				end
			end )
		else
			local plusPath = ":character/images/plus.png" or _
			local createButton = DGS:dgsCreateButton( ((0.17)*(i))-0.08, 0.20, 0.12, 0.36, "", true, char.window[1], 0xFFFFFFFF, 1, 1, plusPath, plusPath, plusPath, tocolor( 211, 211, 211,255 ), 0xFFFFFFFF, 0xFFFFFFFF )
			addEventHandler ( "onDgsMouseClick", createButton, function ( button, state )
				if (state == "up") then
					displayCharacterSelection(true)
					displayCharacterCreation()
				end
			end )
		end
	end
end

addEvent( "onClientInitCharacter", true )
addEventHandler( "onClientInitCharacter", root, function (chars)
	if (chars) then
		for i,v in ipairs(characters) do
			if (chars[i]) then
				characters[i] = chars[i]
			else
				chars[i] = {}
			end
		end
	end
	displayCharacterSelection()
end)

addEvent( "onCharacterCreate", true )
addEventHandler( "onCharacterCreate", root, function ( status )
	-- outputChatBox( tostring(status) )
	if (status) then
		setTooltip("Character berhasil dibuat.")
		destroyElement( char.window[2] )
		triggerServerEvent( "onLoadCharacter", localPlayer )
	else
		setTooltip("Character gagal dibuat, hubungi admin.")
	end
end )

addEvent( "onServerQueryCharacter", true )
addEventHandler( "onServerQueryCharacter", root, function ( msg, status )
	if (msg) then
		if (status == true) then
			setTooltip(msg, 0, 255, 0, 255)
		else
			setTooltip(msg, 255, 0, 0, 255)
		end
	end
end )

addEvent( "onServerSpawnPlayer", true )
addEventHandler( "onServerSpawnPlayer", root, function ( x,y,z )
	showCursor( false )
	displayCharacterSelection(true)
	triggerEvent( ":_stopViewPoint_:", localPlayer )
	-- setElementPosition( localPlayer, x, y, z )
	setCameraTarget(localPlayer)
end )
addEventHandler( "onClientResourceStart", getResourceRootElement(), function (  )
	-- displayCharacterCreation()
end )