addEventHandler( "onVehicleEnter", root, function ( player, seat, jacked )
	setElementData( player, "character.occupiedVehicle", source )
	setElementData( player, "character.occupiedVehicleSeat", seat )
end )

addEventHandler( "onVehicleExit", root, function ( player, seat, jacked )
	setElementData( player, "character.occupiedVehicle", 0 )
	setElementData( player, "character.occupiedVehicleSeat", 0 )
end )