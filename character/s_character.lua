local DB = exports.database

local function saveCharacter(player)
	local x,y,z = getElementPosition( player )
	local rx,ry,rz = getElementRotation( player )
	local dimension = getElementDimension( player )
	local interior = getElementInterior( player )
	local vehicle = getElementData( player, "character.occupiedVehicle")
	local vehicleSeat = getElementData( player, "character.occupiedVehicleSeat")
	local last_area = getElementZoneName( player )
	local id = getElementData( player, "character.id")
	local user_id = getElementData( player, "user.id" )
	local cked = getElementData( player, "character.cked")
	local jailed = getElementData( player, "character.jailed")
	local jailed_id = getElementData( player, "character.jailed_id")
	local last_login = getRealTime().timestamp
	local health = getElementHealth( player )
	local money = getPlayerMoney( player )

	local query = DB:execute("UPDATE characters SET `health` = ??, `last_x` = '??', `last_y` = '??', `last_z` = '??', `last_rot` = '??', `last_dimension` = '??', `last_interior` = '??', `last_vehicle` = '??', `last_seat` = '??', `last_area` = '??', `last_login` = ??, `cash` = ??, `cked` = ??, `jailed` = ??, `jailed_id` = ?? WHERE id = ?? AND user_id = ??",
	 health, x, y, z, rz, dimension, interior, vehicle, vehicleSeat, last_area, last_login, money, cked, jailed, jailed_id, id, user_id)
	if (query) then
		outputServerLog( "CHARACTER: Berhasil menyimpan character "..getPlayerName( player ).." ("..tostring(id)..")" )
	else
		outputServerLog( "WARNING: Terjadi masalah pada koneksi di server. Mohon hubungi admin.", player )
	end
end

addEventHandler( "onPlayerQuit", root, function ( type, reason, responsible )
	if (getElementData( source, "user.isLoggedIn" ) == 1) and (getElementData( source, "user.state" ) == 3) then
		saveCharacter(source)
	end
end )

addEventHandler( "onResourceStop", resourceRoot, function ( )
	for i,v in ipairs(getElementsByType( "player" ) ) do
		if (getElementData( v, "user.isLoggedIn" ) == 1) and (getElementData( v, "user.state" ) == 3) then
			saveCharacter(v)
		end
	end
end )

local function setCharacterData(data, player)
	setElementData( player, "character.id", data["id"] )
	setElementData( player, "character.name", data["name"] )
	setElementData( player, "character.gender", data["gender"] )
	setElementData( player, "character.race", data["race"] )
	setElementData( player, "character.birth_date", data["birth_date"] )
	setElementData( player, "character.origin", data["origin"] )
	setElementData( player, "character.cked", data["cked"] )
	setElementData( player, "character.jailed", data["jailed"] )
	setElementData( player, "character.jailed_id", data["jailed_id"] )
	setElementData( player, "character.last_login", data["last_login"] )
	setElementData( player, "user.state", tonumber(3) )
end

local function getPlayerCharacters(player)
	local id = getElementData( player, "user.id" )
	local character = DB:query("SELECT * FROM `??` WHERE `??` = '??'", "characters", "user_id", id)

	if (character) then
		return character
	end
end

addEvent("onCharacterSelect", true)
addEventHandler( "onCharacterSelect", root, function ( characterId )
	if (source ~= client) then return end
	local id = getElementData( source, "user.id" )
	triggerClientEvent( client, "onServerQueryCharacter", client, "Menunggu respon dari server...", true)
	local character = DB:query_single("SELECT * FROM `??` WHERE `??` = ?? AND `??` = ?? LIMIT 1", "characters", "user_id", id, "id", tonumber(characterId))
	if (character) then
		triggerClientEvent( client, "onServerQueryCharacter", client, "Spawning character...", true)
		if (character.cked == 0) then
			setPlayerName( client, character["name"] )
			spawnPlayer ( client, character["last_x"], character["last_y"], character["last_z"], character["last_rot"], character["skin"], character["last_interior"], character["last_dimension"] )
			if (tonumber(character["last_vehicle"]) > 3) then
				-- spawn to veh warpPedIntoVehicle(ped, veh, seat)
			end
			setPlayerMoney( client, character["cash"] )
			setCharacterData(character, client)
			setCameraTarget(client, client)
			triggerClientEvent( client, "onServerSpawnPlayer", client, character["last_x"], character["last_y"], character["last_z"] )
		else
			triggerClientEvent( client, "onServerQueryCharacter", client, "The character is killed.", false)

		end
	end
end )

addEvent( "onLoadCharacter", true )
addEventHandler( "onLoadCharacter", root, function (  )
	local characters = getPlayerCharacters(source)
	triggerClientEvent( source, "onClientInitCharacter", source, characters )
end )

addEvent( "onClientSubmitNewCharacter", true )
addEventHandler( "onClientSubmitNewCharacter", root, function ( playerSelectedModel, characterName, characterGender, characterRace, characterBirthday, characterBio, characterOrigin )
	if (source ~= client) then return end
	local id = getElementData( client, "user.id" )
	triggerClientEvent( client, "onServerQueryCharacter", client, "Menunggu respon dari server...", true)
	local connection = DB:getConnection()
	local query = DB:execute("INSERT INTO characters (user_id, skin, name, gender, race, birthDate, description, origin, cash, date_created) VALUES (?,?,?,?,?,?,?,?,?,?)", id, playerSelectedModel, characterName:gsub(" ", "_"), characterGender, characterRace, characterBirthday, characterBio, characterOrigin, 800, getRealTime().timestamp)
	if (query) then
		triggerClientEvent( client, "onCharacterCreate", resourceRoot, true )
	else
		triggerClientEvent( client, "onCharacterCreate", resourceRoot, false )
	end
end )

addEventHandler("onPlayerChangeNick", root,
	function(old, new, userChanged)
		if (userChanged) then
			cancelEvent()
			outputChatBox("Sorry, but we do not allow the change of nicknames on our server. Disconnect and change your nickname then instead.", source, 245, 20, 20, false)
		end
	end
)

addEventHandler( "onElementDataChange", root, function ( dataName )
	if (getElementType(source) == "player") then
		if (dataName == "user.state") then
			if (getElementData( source, "user.state" ) == 2) then
				triggerEvent( "onLoadCharacter", source )
			end
		end
	end
end )