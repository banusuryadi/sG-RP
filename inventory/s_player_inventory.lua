local DB = exports.database

function loadPlayerItems( player )
	local id = getElementData( player, "character.id" )
	local result, num_affected_rows, last_insert_id = DB:query( "SELECT * FROM inventory WHERE `ownerId` = '??'", id )
	if (result) then
		triggerClientEvent( player, "onServerLoadItems", player, result)
	end
end

function givePlayerItem( player, itemId, quantity, value )
	local id = getElementData( player, "character.id" )

	local item = getItem(tonumber(itemId))
	if (item) then
		if (not quantity) then
			quantity = 1
		end
		if (not value) then
			value = ""
		end
		local result = DB:query( "INSERT INTO inventory (`itemId`,`ownerId`,`quantity`,`value`) VALUES ('??','??','??','??')", itemId, id, quantity, value )
		if (result) then
			loadPlayerItems( player )
			return true
		end
	else
		return false
	end
end

function takePlayerItem( player, dbId, quantity, value )
	local id = getElementData( player, "character.id" )
	
	local item = getPlayerItemFromDbId(dbId)
	if (quantity) then
		-- return
	end

	if (item) then
		local result = DB:query_single( "DELETE FROM inventory where `id` = ??" , dbId )
		if (result) then
			loadPlayerItems(player)
			return true
		else
			return false
		end
	else
		return false
	end
end

function getPlayerItemFromDbId( dbId )
	local dbId = tonumber(dbId)
	local item = DB:query( "SELECT * FROM `inventory` where `id` = ??" , dbId )
	if (item) then
		return item
	end
	return false
end

function hasItem( player, itemId, value, dbId )
	if (not getItem(itemId)) then return false end
	if (dbId) then
		local item = DB:query_single( "SELECT * FROM `inventory` where `id` = ??" , dbId )
		if (item) then return true end
		return false
	end

	local ownerId = getElementData(player, "character.id")
	local item = DB:query_single( "SELECT * FROM `inventory` where `ownerId` = ?? AND `itemId` = ?? AND `value` = ??" , tonumber(ownerId), tonumber(itemId), value )
	if (item) then return true end
	return false

end

addEvent("onPlayerDropItem", true)
addEventHandler( "onPlayerDropItem", root, function ( dbId, quantity )
	if (source ~= client) then return end
	takePlayerItem(source, dbId, quantity)
end )

addEvent("onPlayerGiveItemToVehicle", true)
addEventHandler( "onPlayerGiveItemToVehicle", root, function ( vehicle, data, quantity )
	if (source ~= client) then return end
	local dbId = data.id
	local value = data.value
	local takenItem = takePlayerItem(source, dbId, quantity)
	if (takenItem) then	
		giveVehicleItem( vehicle, source, data.itemId, quantity, value )
	end
end )

addEvent("onPlayerTakeItemFromVehicle", true)
addEventHandler( "onPlayerTakeItemFromVehicle", root, function ( vehicle, data, quantity )
	if (source ~= client) then return end
	local dbId = data.id
	local value = data.value
	local takenItem = takeVehicleItem(vehicle, source, dbId, quantity)
	if (takenItem) then	
		givePlayerItem( source, data.itemId, quantity, value )
	end
end )

addEvent( "onPlayerInventoryShow", true )
addEventHandler( "onPlayerInventoryShow", root, function (  )
	if (source ~= client) then return end
	loadPlayerItems(source)
end )

-- commands

local addCommandHandler_ = addCommandHandler
	addCommandHandler = function(commandName, fn, restricted, caseSensitive)
	if (type(commandName) ~= "table") then
		commandName = {commandName}
	end
	for key, value in ipairs(commandName) do
		if (key == 1) then
			addCommandHandler_(value, fn, restricted, false)
		else
			addCommandHandler_(value,
				function(player, ...)
					fn(player, ...)
				end, false, false
			)
		end
	end
end
local CHARACTER = exports.character
addCommandHandler( {"giveitem"}, 
	function ( player, commandName, characterId, itemId, quantity, value )
		local admin_level = getElementData( player, "user.adminLevel" )
		if (admin_level) then
			if (tonumber(admin_level) > 4) then
				if (characterId) and (itemId) then
					local targetPlayer = CHARACTER:getPlayerByCharacterId(tonumber(characterId))
					if (targetPlayer) then
						if (not quantity) then
							quantity = 1
						end
						if (not value) then
							value = ""
						end
						local given = givePlayerItem( targetPlayer, itemId, quantity, value)
						if (given) then
							outputChatBox( "Berhasil memberikan item ("..itemId..") kepada character id ("..characterId..")" , player, 0,255,0 )
						end
					else
						outputChatBox( "Character tidak ditemukan.", player, 255,0,0 )
					end
				else
					outputChatBox( "/"..commandName.." [characterId] [itemId] [quantity*] [value*]", player, 3,169,244 )
				end
			end
		end
	end 
)