local DB = exports.database
function loadVehicleItems( vehicle, player )
	local id = getElementData( vehicle, "vehicle.id" )
	if (id) then
		local result, num_affected_rows, last_insert_id = DB:query( "SELECT * FROM vehicle_inventory WHERE `vehicleId` = '??'", id )
		if (result) then
			triggerClientEvent( player, "onServerLoadVehicleItems", resourceRoot, result)
		end
	end
end

function giveVehicleItem( vehicle, player, itemId, quantity, value )
	local id = getElementData( vehicle, "vehicle.id" )

	local item = getItem(tonumber(itemId))
	if (item) then
		if (not quantity) then
			quantity = 1
		end
		if (not value) then
			value = ""
		end
		local result = DB:query( "INSERT INTO vehicle_inventory (`itemId`,`vehicleId`,`quantity`,`value`) VALUES ('??','??','??','??')", itemId, id, quantity, value )
		if (result) then
			loadVehicleItems( vehicle, player )
			return true
		end
	else
		return false
	end
end


function getVehicleItemFromDbId( dbId )
	local dbId = tonumber(dbId)
	local item = DB:query( "SELECT * FROM `vehicle_inventory` where `id` = ??" , dbId )
	if (item) then
		return item
	end
	return false
end

function takeVehicleItem( vehicle, player, dbId, quantity, value )
	local id = getElementData( vehicle, "vehicle.id" )
	
	local item = getVehicleItemFromDbId(dbId)
	if (quantity) then
		-- return
	end

	if (item) then
		local result = DB:query_single( "DELETE FROM vehicle_inventory where `id` = ??" , dbId )
		if (result) then
			loadVehicleItems( vehicle, player )
			return true
		else
			return false
		end
	else
		return false
	end
end

addEvent( "onVehicleInventoryShow", true )
addEventHandler( "onVehicleInventoryShow", root, function ( vehicle )
	if (source ~= client) then return end
	loadVehicleItems(vehicle, source)
end )