local sw, sh = guiGetScreenSize(  )
local gw, gh = 1024, 768
local inv = {
	window = {},
	label = {},
	scrollpane = {}
}
local items = {
	--[id] = {item}
}
local vehicleItems = {
	--[id] = {item}
}
local DGS = exports.dgs
local vehicleColShape
local isNearVeh = nil
local iconData = {}

local function loadPlayerInventory( )
	if (not isElement( inv.window[2] )) then return end
	for i,v in ipairs(DGS:dgsGetChildren(inv.scrollpane[1])) do
		if (isElement (v)) then
			destroyElement( v )
		end
	end
	local column = 0
	local rows = 0
	
	for i,item in ipairs(items) do
		local sx = 0.05
		local sy = 0.05
		local iconPath = dxCreateTexture( "images/"..item.itemId..".png" ) or _
		-- local icon = DGS:dgsCreateImage(sx+(0.185*(column)), sy+(0.185*(rows)), 0.15, 0.15, iconPath, true, inv.window[2])
		local icon = DGS:dgsCreateButton(sx+(0.185*(column)), sy+(0.185*(rows)), 0.15, 0.15, "", true, inv.scrollpane[1], _, _, _, iconPath, iconPath, iconPath, tocolor( 211, 211, 211,255 ), tocolor(255,255,255,255), tocolor( 211, 211, 211, 255 ))
		iconData[icon] = item
		if (i%5==0) then
			column = 0
			rows = rows + 1
		else
			column = column + 1
		end
	end
end


local tooltip
local hoveringElement = nil
local draggingItem
local draggingData

addEventHandler( "onDgsMouseEnter", root, function ( )
	hoveringElement = source
	if (iconData[source]) then
		if (DGS:dgsGetParent(source) == inv.scrollpane[1] ) then
			local itemName = getItemName(iconData[source].itemId)
			DGS:dgsGUISetText(inv.label[10], itemName.." ( ID: "..iconData[source].value.." )")
		end
	end
end )

addEventHandler( "onDgsMouseLeave", root, function (  )
	hoveringElement = nil
	if (iconData[source]) then
		if (DGS:dgsGetParent(source) == inv.scrollpane[1] ) then
			DGS:dgsGUISetText(inv.label[10], itemName.." ( ID: "..iconData[source].value.." )")
		end
	end
end )

addEventHandler( "onDgsMouseClick", root, function ( button, state, x, y)
	if (iconData[source]) then
		if (state == "down") and (button == "left") then
			dragTimer = setTimer( function ( item )
				local iconPath = dxCreateTexture( "images/"..item.itemId..".png" ) or _
				draggingItem = DGS:dgsCreateImage(x+5, y+5, 50, 50, iconPath, false)
				draggingData = item
			end, 150, 1, iconData[source] )
		else
			if (isTimer( dragTimer )) then
				killTimer( dragTimer )
			end
		end
	end
end )

addEventHandler( "onClientCursorMove", getRootElement( ), function ( _, _, x, y )
	if (isElement( draggingItem )) then
		DGS:dgsSetPosition(draggingItem, x+5, y+5)
	end
end )

addEventHandler( "onClientClick", root, function ( button, state, x ,y )
	if (state == "up") and (button == "left") then
		if (isElement( draggingItem )) then
			destroyElement( draggingItem )
			if (not isElement(hoveringElement) ) then
				triggerServerEvent( "onPlayerDropItem", localPlayer, draggingData.id)
			else
				if(hoveringElement == inv.scrollpane[2] or (iconData[hoveringElement] and (DGS:dgsGetParent(hoveringElement) == inv.scrollpane[2]))) then
					if (isNearVeh) then
						triggerServerEvent( "onPlayerGiveItemToVehicle", localPlayer, isNearVeh, draggingData)
					-- else
					-- 	outputChatBox( "Terlalu jauh dari kendaraan." )
					end
				end
				if(hoveringElement == inv.scrollpane[1] or (iconData[hoveringElement] and (DGS:dgsGetParent(hoveringElement) == inv.scrollpane[1]))) then
					if (isNearVeh) then
						triggerServerEvent( "onPlayerTakeItemFromVehicle", localPlayer, isNearVeh, draggingData)
					-- else
					-- 	outputChatBox( "Terlalu jauh dari kendaraan." )
					end
				end
			end
		end

		if (isTimer( dragTimer )) then
			killTimer( dragTimer )
		end

	end
end )

local function loadVehicleInventory(vehicle)
	if (not isElement( inv.window[3] )) then return end
	for i,v in ipairs(DGS:dgsGetChildren(inv.scrollpane[2])) do
		if (isElement (v)) then
			destroyElement( v )
		end
	end
	local column = 0
	local rows = 0
	
	for i,item in ipairs(vehicleItems) do
		local sx = 0.05
		local sy = 0.05
		local iconPath = dxCreateTexture( "images/"..item.itemId..".png" ) or _
		-- local icon = DGS:dgsCreateImage(sx+(0.185*(column)), sy+(0.185*(rows)), 0.15, 0.15, iconPath, true, inv.window[2])
		local icon = DGS:dgsCreateButton(sx+(0.185*(column)), sy+(0.185*(rows)), 0.15, 0.15, "", true, inv.scrollpane[2], _, _, _, iconPath, iconPath, iconPath, tocolor( 211, 211, 211,255 ), tocolor(255,255,255,255), tocolor( 211, 211, 211, 255 ))
		iconData[icon] = item
		if (i%5==0) then
			column = 0
			rows = rows + 1
		else
			column = column + 1
		end
	end
end

local function showInventory()
	if (isElement( inv.window[2] )) then
		
		for i,v in ipairs(DGS:dgsGetDxGUIFromResource()) do
			if (isElement (v)) then
				destroyElement( v )
			end
		end
		showCursor( false )

		return
	end
	
	triggerServerEvent( "onPlayerInventoryShow", localPlayer )

	showCursor( true )
	-- inv.window[1] = DGS:dgsCreateWindow(sw*0.6, sh*0.05, sw*0.3, sh*0.4, "Character", false, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x96141414, 5, true)
	-- inv.label[1] = DGS:dgsCreateLabel(0.05, 0.05, 1, 1, "Head", true, inv.window[1])
	-- inv.label[2] = DGS:dgsCreateLabel(0.05, 0.1, 1, 1, "Body", true, inv.window[1])
	-- inv.label[3] = DGS:dgsCreateLabel(0.05, 0.15, 1, 1, "Weapon 1", true, inv.window[1])
	-- inv.label[4] = DGS:dgsCreateLabel(0.05, 0.2, 1, 1, "Weapon 2", true, inv.window[1])
	-- inv.label[5] = DGS:dgsCreateLabel(0.05, 0.25, 1, 1, "Secondary", true, inv.window[1])
	-- inv.label[6] = DGS:dgsCreateLabel(0.05, 0.3, 1, 1, "Melee", true, inv.window[1])
	-- inv.label[7] = DGS:dgsCreateLabel(0.05, 0.35, 1, 1, "Utilites", true, inv.window[1])
	-- inv.label[8] = DGS:dgsCreateLabel(0.05, 0.4, 1, 1, "Accessories", true, inv.window[1])
	-- inv.label[9] = DGS:dgsCreateLabel(0.05, 0.45, 1, 1, "((Skin))", true, inv.window[1])

	inv.window[2] = DGS:dgsCreateWindow(sw*0.6, sh*0.45, sw*0.3, sh*0.4, "Inventory", false, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x96141414, 5, true)
	inv.scrollpane[1] =  DGS:dgsCreateScrollPane( 0, 0, 1, 0.9, true, inv.window[2])
	inv.label[10] = DGS:dgsCreateLabel(0.05, 0.9, 1, 0.9, "", true, inv.window[2])

	loadPlayerInventory()
	if (isElement( isNearVeh ) and (not isVehicleLocked(isNearVeh))) or (getPedOccupiedVehicle(localPlayer)) then
		loadVehicleInventory(isNearVeh)
		local vehicle = getPedOccupiedVehicle(localPlayer) or isNearVeh
		local vehid = getElementData( vehicle, "vehicle.id")
		inv.window[3] = DGS:dgsCreateWindow(sw*0.3, sh*0.45, sw*0.28, sh*0.4, getVehicleName(vehicle).." ("..vehid..")", false, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x96141414, 5, true)
		inv.scrollpane[2] =  DGS:dgsCreateScrollPane( 0, 0, 1, 0.9, true, inv.window[3])
		inv.label[11] = DGS:dgsCreateLabel(0.05, 0.9, 1, 0.9, "", true, inv.window[3])
		triggerServerEvent( "onVehicleInventoryShow", localPlayer, vehicle )
	end
end

addEvent( "onServerLoadItems", true )
addEventHandler( "onServerLoadItems", root, function ( playerItems )
	items = {}
	for id,item in ipairs( playerItems ) do
		items[id] = item
	end
	loadPlayerInventory()
end )

addEvent( "onServerLoadVehicleItems", true )
addEventHandler( "onServerLoadVehicleItems", root, function ( vehItems )
	vehicleItems = {}
	for id,item in ipairs( vehItems ) do
		vehicleItems[id] = item
	end
	loadVehicleInventory()
end )

addEventHandler( "onClientPlayerSpawn", localPlayer, function ( )
	bindKey("tab", "down", showInventory)
	local x, y, z = getElementPosition( source )
	vehicleColShape = createColSphere( x, y, z, 2.2 )
	attachElements( vehicleColShape, localPlayer )
end )

addEventHandler( "onClientResourceStart", root, function ( )
	if (getElementData( localPlayer, "user.state" ) == 3) then
		unbindKey( "tab", "down", showInventory)
		bindKey( "tab", "down", showInventory)
		if (not isElement(vehicleColShape)) then
			local x, y, z = getElementPosition( localPlayer )
			vehicleColShape = createColSphere( x, y, z, 2.2 )
			attachElements( vehicleColShape, localPlayer )
		end
	end
end )

addEventHandler( "onClientColShapeHit", root, function ( hitElement, matchingDimension )
	if(source == vehicleColShape) then
		if (getElementType( hitElement ) == "vehicle") then
			isNearVeh = hitElement
		end
	end
end )

addEventHandler( "onClientColShapeLeave", root, function ( hitElement, matchingDimension )
	if(source == vehicleColShape) then
		if (getElementType( hitElement ) == "vehicle") then
			if(isNearVeh == hitElement)then
				isNearVeh = nil
			end
		end
	end
end )

addEventHandler( "onElementDataChange", root, function ( dataName )
	if (getElementType(source) == "player") then
		if (dataName == "user.state") then
			if (getElementData( source, "user.state" ) == 3) then
				bindKey( "tab", "down", showInventory)
			else
				unbindKey("tab", "down", showInventory)
			end
		end
	end
end )