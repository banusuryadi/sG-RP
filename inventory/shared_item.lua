local items = {
	-- name, weight, size, collision, alpha, desc
	[1]	= {"Kunci Kendaraan",	1,	1,	0,	255, "Sebuah kunci kendaraan"},
	[2]	= {"Kunci Interior",	1,	1,	0,	255, "Sebuah kunci pintu"}
}

function getItem( itemId )
	if (itemId) then
		return items[itemId]
	end
	return false
end

function getItemName( itemId )
	if (itemId) then
		return items[itemId][1]
	end
	return false
end