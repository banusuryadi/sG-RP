/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : sg_rp

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2017-11-05 23:32:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin_level
-- ----------------------------
DROP TABLE IF EXISTS `admin_level`;
CREATE TABLE `admin_level` (
  `id` smallint(6) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_level
-- ----------------------------
INSERT INTO `admin_level` VALUES ('0', 'Player');
INSERT INTO `admin_level` VALUES ('1', 'Trial Support');
INSERT INTO `admin_level` VALUES ('2', 'Team Support');
INSERT INTO `admin_level` VALUES ('3', 'Lead Team Support');
INSERT INTO `admin_level` VALUES ('4', 'Trial Admin');
INSERT INTO `admin_level` VALUES ('5', 'Admin');
INSERT INTO `admin_level` VALUES ('6', 'Lead Admin');
INSERT INTO `admin_level` VALUES ('7', 'Super Admin');
INSERT INTO `admin_level` VALUES ('99', 'Scripter');

-- ----------------------------
-- Table structure for characters
-- ----------------------------
DROP TABLE IF EXISTS `characters`;
CREATE TABLE `characters` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `gender` tinyint(2) DEFAULT NULL,
  `health` int(100) DEFAULT '100',
  `last_x` double DEFAULT NULL,
  `last_y` double DEFAULT NULL,
  `last_z` double DEFAULT NULL,
  `last_rot` double DEFAULT NULL,
  `last_dimension` varchar(255) DEFAULT NULL,
  `last_interior` varchar(255) DEFAULT NULL,
  `last_vehicle` varchar(45) DEFAULT '0',
  `last_seat` varchar(45) DEFAULT '0',
  `last_area` varchar(255) DEFAULT NULL,
  `last_login` int(11) DEFAULT NULL,
  `dateCreated` int(11) DEFAULT NULL,
  `skin` varchar(255) DEFAULT NULL,
  `race` varchar(255) DEFAULT NULL,
  `birth_date` varchar(255) DEFAULT '01/01/1990',
  `cash` bigint(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `cked` tinyint(2) DEFAULT '0',
  `origin` int(11) DEFAULT NULL,
  `jailed` tinyint(2) DEFAULT '0',
  `jailedId` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user` (`user_id`),
  CONSTRAINT `user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of characters
-- ----------------------------
INSERT INTO `characters` VALUES ('14', '1', 'Banu_Suryadi', '1', '100', '-2581.245117', '680.550781', '27.8125', '193.678192', '0', '0', '0', '0', 'Santa Flora', '1509884963', '1505373840', '70', '1', '12/12/1995', '800', 'DELETE FROM table_name;DELETE FROM table_name;DELETE FROM table_name;DELETE FROM table_namDELETE FROM table_name;e;', '0', '230', '0', '0');

-- ----------------------------
-- Table structure for countries
-- ----------------------------
DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=246 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of countries
-- ----------------------------
INSERT INTO `countries` VALUES ('1', 'AF', 'Afghanistan');
INSERT INTO `countries` VALUES ('2', 'AL', 'Albania');
INSERT INTO `countries` VALUES ('3', 'DZ', 'Algeria');
INSERT INTO `countries` VALUES ('4', 'DS', 'American Samoa');
INSERT INTO `countries` VALUES ('5', 'AD', 'Andorra');
INSERT INTO `countries` VALUES ('6', 'AO', 'Angola');
INSERT INTO `countries` VALUES ('7', 'AI', 'Anguilla');
INSERT INTO `countries` VALUES ('8', 'AQ', 'Antarctica');
INSERT INTO `countries` VALUES ('9', 'AG', 'Antigua and Barbuda');
INSERT INTO `countries` VALUES ('10', 'AR', 'Argentina');
INSERT INTO `countries` VALUES ('11', 'AM', 'Armenia');
INSERT INTO `countries` VALUES ('12', 'AW', 'Aruba');
INSERT INTO `countries` VALUES ('13', 'AU', 'Australia');
INSERT INTO `countries` VALUES ('14', 'AT', 'Austria');
INSERT INTO `countries` VALUES ('15', 'AZ', 'Azerbaijan');
INSERT INTO `countries` VALUES ('16', 'BS', 'Bahamas');
INSERT INTO `countries` VALUES ('17', 'BH', 'Bahrain');
INSERT INTO `countries` VALUES ('18', 'BD', 'Bangladesh');
INSERT INTO `countries` VALUES ('19', 'BB', 'Barbados');
INSERT INTO `countries` VALUES ('20', 'BY', 'Belarus');
INSERT INTO `countries` VALUES ('21', 'BE', 'Belgium');
INSERT INTO `countries` VALUES ('22', 'BZ', 'Belize');
INSERT INTO `countries` VALUES ('23', 'BJ', 'Benin');
INSERT INTO `countries` VALUES ('24', 'BM', 'Bermuda');
INSERT INTO `countries` VALUES ('25', 'BT', 'Bhutan');
INSERT INTO `countries` VALUES ('26', 'BO', 'Bolivia');
INSERT INTO `countries` VALUES ('27', 'BA', 'Bosnia and Herzegovina');
INSERT INTO `countries` VALUES ('28', 'BW', 'Botswana');
INSERT INTO `countries` VALUES ('29', 'BV', 'Bouvet Island');
INSERT INTO `countries` VALUES ('30', 'BR', 'Brazil');
INSERT INTO `countries` VALUES ('31', 'IO', 'British Indian Ocean Territory');
INSERT INTO `countries` VALUES ('32', 'BN', 'Brunei Darussalam');
INSERT INTO `countries` VALUES ('33', 'BG', 'Bulgaria');
INSERT INTO `countries` VALUES ('34', 'BF', 'Burkina Faso');
INSERT INTO `countries` VALUES ('35', 'BI', 'Burundi');
INSERT INTO `countries` VALUES ('36', 'KH', 'Cambodia');
INSERT INTO `countries` VALUES ('37', 'CM', 'Cameroon');
INSERT INTO `countries` VALUES ('38', 'CA', 'Canada');
INSERT INTO `countries` VALUES ('39', 'CV', 'Cape Verde');
INSERT INTO `countries` VALUES ('40', 'KY', 'Cayman Islands');
INSERT INTO `countries` VALUES ('41', 'CF', 'Central African Republic');
INSERT INTO `countries` VALUES ('42', 'TD', 'Chad');
INSERT INTO `countries` VALUES ('43', 'CL', 'Chile');
INSERT INTO `countries` VALUES ('44', 'CN', 'China');
INSERT INTO `countries` VALUES ('45', 'CX', 'Christmas Island');
INSERT INTO `countries` VALUES ('46', 'CC', 'Cocos (Keeling) Islands');
INSERT INTO `countries` VALUES ('47', 'CO', 'Colombia');
INSERT INTO `countries` VALUES ('48', 'KM', 'Comoros');
INSERT INTO `countries` VALUES ('49', 'CG', 'Congo');
INSERT INTO `countries` VALUES ('50', 'CK', 'Cook Islands');
INSERT INTO `countries` VALUES ('51', 'CR', 'Costa Rica');
INSERT INTO `countries` VALUES ('52', 'HR', 'Croatia (Hrvatska)');
INSERT INTO `countries` VALUES ('53', 'CU', 'Cuba');
INSERT INTO `countries` VALUES ('54', 'CY', 'Cyprus');
INSERT INTO `countries` VALUES ('55', 'CZ', 'Czech Republic');
INSERT INTO `countries` VALUES ('56', 'DK', 'Denmark');
INSERT INTO `countries` VALUES ('57', 'DJ', 'Djibouti');
INSERT INTO `countries` VALUES ('58', 'DM', 'Dominica');
INSERT INTO `countries` VALUES ('59', 'DO', 'Dominican Republic');
INSERT INTO `countries` VALUES ('60', 'TP', 'East Timor');
INSERT INTO `countries` VALUES ('61', 'EC', 'Ecuador');
INSERT INTO `countries` VALUES ('62', 'EG', 'Egypt');
INSERT INTO `countries` VALUES ('63', 'SV', 'El Salvador');
INSERT INTO `countries` VALUES ('64', 'GQ', 'Equatorial Guinea');
INSERT INTO `countries` VALUES ('65', 'ER', 'Eritrea');
INSERT INTO `countries` VALUES ('66', 'EE', 'Estonia');
INSERT INTO `countries` VALUES ('67', 'ET', 'Ethiopia');
INSERT INTO `countries` VALUES ('68', 'FK', 'Falkland Islands (Malvinas)');
INSERT INTO `countries` VALUES ('69', 'FO', 'Faroe Islands');
INSERT INTO `countries` VALUES ('70', 'FJ', 'Fiji');
INSERT INTO `countries` VALUES ('71', 'FI', 'Finland');
INSERT INTO `countries` VALUES ('72', 'FR', 'France');
INSERT INTO `countries` VALUES ('73', 'FX', 'France, Metropolitan');
INSERT INTO `countries` VALUES ('74', 'GF', 'French Guiana');
INSERT INTO `countries` VALUES ('75', 'PF', 'French Polynesia');
INSERT INTO `countries` VALUES ('76', 'TF', 'French Southern Territories');
INSERT INTO `countries` VALUES ('77', 'GA', 'Gabon');
INSERT INTO `countries` VALUES ('78', 'GM', 'Gambia');
INSERT INTO `countries` VALUES ('79', 'GE', 'Georgia');
INSERT INTO `countries` VALUES ('80', 'DE', 'Germany');
INSERT INTO `countries` VALUES ('81', 'GH', 'Ghana');
INSERT INTO `countries` VALUES ('82', 'GI', 'Gibraltar');
INSERT INTO `countries` VALUES ('83', 'GK', 'Guernsey');
INSERT INTO `countries` VALUES ('84', 'GR', 'Greece');
INSERT INTO `countries` VALUES ('85', 'GL', 'Greenland');
INSERT INTO `countries` VALUES ('86', 'GD', 'Grenada');
INSERT INTO `countries` VALUES ('87', 'GP', 'Guadeloupe');
INSERT INTO `countries` VALUES ('88', 'GU', 'Guam');
INSERT INTO `countries` VALUES ('89', 'GT', 'Guatemala');
INSERT INTO `countries` VALUES ('90', 'GN', 'Guinea');
INSERT INTO `countries` VALUES ('91', 'GW', 'Guinea-Bissau');
INSERT INTO `countries` VALUES ('92', 'GY', 'Guyana');
INSERT INTO `countries` VALUES ('93', 'HT', 'Haiti');
INSERT INTO `countries` VALUES ('94', 'HM', 'Heard and Mc Donald Islands');
INSERT INTO `countries` VALUES ('95', 'HN', 'Honduras');
INSERT INTO `countries` VALUES ('96', 'HK', 'Hong Kong');
INSERT INTO `countries` VALUES ('97', 'HU', 'Hungary');
INSERT INTO `countries` VALUES ('98', 'IS', 'Iceland');
INSERT INTO `countries` VALUES ('99', 'IN', 'India');
INSERT INTO `countries` VALUES ('100', 'IM', 'Isle of Man');
INSERT INTO `countries` VALUES ('101', 'ID', 'Indonesia');
INSERT INTO `countries` VALUES ('102', 'IR', 'Iran (Islamic Republic of)');
INSERT INTO `countries` VALUES ('103', 'IQ', 'Iraq');
INSERT INTO `countries` VALUES ('104', 'IE', 'Ireland');
INSERT INTO `countries` VALUES ('105', 'IL', 'Israel');
INSERT INTO `countries` VALUES ('106', 'IT', 'Italy');
INSERT INTO `countries` VALUES ('107', 'CI', 'Ivory Coast');
INSERT INTO `countries` VALUES ('108', 'JE', 'Jersey');
INSERT INTO `countries` VALUES ('109', 'JM', 'Jamaica');
INSERT INTO `countries` VALUES ('110', 'JP', 'Japan');
INSERT INTO `countries` VALUES ('111', 'JO', 'Jordan');
INSERT INTO `countries` VALUES ('112', 'KZ', 'Kazakhstan');
INSERT INTO `countries` VALUES ('113', 'KE', 'Kenya');
INSERT INTO `countries` VALUES ('114', 'KI', 'Kiribati');
INSERT INTO `countries` VALUES ('115', 'KP', 'Korea, Democratic People\'s Republic of');
INSERT INTO `countries` VALUES ('116', 'KR', 'Korea, Republic of');
INSERT INTO `countries` VALUES ('117', 'XK', 'Kosovo');
INSERT INTO `countries` VALUES ('118', 'KW', 'Kuwait');
INSERT INTO `countries` VALUES ('119', 'KG', 'Kyrgyzstan');
INSERT INTO `countries` VALUES ('120', 'LA', 'Lao People\'s Democratic Republic');
INSERT INTO `countries` VALUES ('121', 'LV', 'Latvia');
INSERT INTO `countries` VALUES ('122', 'LB', 'Lebanon');
INSERT INTO `countries` VALUES ('123', 'LS', 'Lesotho');
INSERT INTO `countries` VALUES ('124', 'LR', 'Liberia');
INSERT INTO `countries` VALUES ('125', 'LY', 'Libyan Arab Jamahiriya');
INSERT INTO `countries` VALUES ('126', 'LI', 'Liechtenstein');
INSERT INTO `countries` VALUES ('127', 'LT', 'Lithuania');
INSERT INTO `countries` VALUES ('128', 'LU', 'Luxembourg');
INSERT INTO `countries` VALUES ('129', 'MO', 'Macau');
INSERT INTO `countries` VALUES ('130', 'MK', 'Macedonia');
INSERT INTO `countries` VALUES ('131', 'MG', 'Madagascar');
INSERT INTO `countries` VALUES ('132', 'MW', 'Malawi');
INSERT INTO `countries` VALUES ('133', 'MY', 'Malaysia');
INSERT INTO `countries` VALUES ('134', 'MV', 'Maldives');
INSERT INTO `countries` VALUES ('135', 'ML', 'Mali');
INSERT INTO `countries` VALUES ('136', 'MT', 'Malta');
INSERT INTO `countries` VALUES ('137', 'MH', 'Marshall Islands');
INSERT INTO `countries` VALUES ('138', 'MQ', 'Martinique');
INSERT INTO `countries` VALUES ('139', 'MR', 'Mauritania');
INSERT INTO `countries` VALUES ('140', 'MU', 'Mauritius');
INSERT INTO `countries` VALUES ('141', 'TY', 'Mayotte');
INSERT INTO `countries` VALUES ('142', 'MX', 'Mexico');
INSERT INTO `countries` VALUES ('143', 'FM', 'Micronesia, Federated States of');
INSERT INTO `countries` VALUES ('144', 'MD', 'Moldova, Republic of');
INSERT INTO `countries` VALUES ('145', 'MC', 'Monaco');
INSERT INTO `countries` VALUES ('146', 'MN', 'Mongolia');
INSERT INTO `countries` VALUES ('147', 'ME', 'Montenegro');
INSERT INTO `countries` VALUES ('148', 'MS', 'Montserrat');
INSERT INTO `countries` VALUES ('149', 'MA', 'Morocco');
INSERT INTO `countries` VALUES ('150', 'MZ', 'Mozambique');
INSERT INTO `countries` VALUES ('151', 'MM', 'Myanmar');
INSERT INTO `countries` VALUES ('152', 'NA', 'Namibia');
INSERT INTO `countries` VALUES ('153', 'NR', 'Nauru');
INSERT INTO `countries` VALUES ('154', 'NP', 'Nepal');
INSERT INTO `countries` VALUES ('155', 'NL', 'Netherlands');
INSERT INTO `countries` VALUES ('156', 'AN', 'Netherlands Antilles');
INSERT INTO `countries` VALUES ('157', 'NC', 'New Caledonia');
INSERT INTO `countries` VALUES ('158', 'NZ', 'New Zealand');
INSERT INTO `countries` VALUES ('159', 'NI', 'Nicaragua');
INSERT INTO `countries` VALUES ('160', 'NE', 'Niger');
INSERT INTO `countries` VALUES ('161', 'NG', 'Nigeria');
INSERT INTO `countries` VALUES ('162', 'NU', 'Niue');
INSERT INTO `countries` VALUES ('163', 'NF', 'Norfolk Island');
INSERT INTO `countries` VALUES ('164', 'MP', 'Northern Mariana Islands');
INSERT INTO `countries` VALUES ('165', 'NO', 'Norway');
INSERT INTO `countries` VALUES ('166', 'OM', 'Oman');
INSERT INTO `countries` VALUES ('167', 'PK', 'Pakistan');
INSERT INTO `countries` VALUES ('168', 'PW', 'Palau');
INSERT INTO `countries` VALUES ('169', 'PS', 'Palestine');
INSERT INTO `countries` VALUES ('170', 'PA', 'Panama');
INSERT INTO `countries` VALUES ('171', 'PG', 'Papua New Guinea');
INSERT INTO `countries` VALUES ('172', 'PY', 'Paraguay');
INSERT INTO `countries` VALUES ('173', 'PE', 'Peru');
INSERT INTO `countries` VALUES ('174', 'PH', 'Philippines');
INSERT INTO `countries` VALUES ('175', 'PN', 'Pitcairn');
INSERT INTO `countries` VALUES ('176', 'PL', 'Poland');
INSERT INTO `countries` VALUES ('177', 'PT', 'Portugal');
INSERT INTO `countries` VALUES ('178', 'PR', 'Puerto Rico');
INSERT INTO `countries` VALUES ('179', 'QA', 'Qatar');
INSERT INTO `countries` VALUES ('180', 'RE', 'Reunion');
INSERT INTO `countries` VALUES ('181', 'RO', 'Romania');
INSERT INTO `countries` VALUES ('182', 'RU', 'Russian Federation');
INSERT INTO `countries` VALUES ('183', 'RW', 'Rwanda');
INSERT INTO `countries` VALUES ('184', 'KN', 'Saint Kitts and Nevis');
INSERT INTO `countries` VALUES ('185', 'LC', 'Saint Lucia');
INSERT INTO `countries` VALUES ('186', 'VC', 'Saint Vincent and the Grenadines');
INSERT INTO `countries` VALUES ('187', 'WS', 'Samoa');
INSERT INTO `countries` VALUES ('188', 'SM', 'San Marino');
INSERT INTO `countries` VALUES ('189', 'ST', 'Sao Tome and Principe');
INSERT INTO `countries` VALUES ('190', 'SA', 'Saudi Arabia');
INSERT INTO `countries` VALUES ('191', 'SN', 'Senegal');
INSERT INTO `countries` VALUES ('192', 'RS', 'Serbia');
INSERT INTO `countries` VALUES ('193', 'SC', 'Seychelles');
INSERT INTO `countries` VALUES ('194', 'SL', 'Sierra Leone');
INSERT INTO `countries` VALUES ('195', 'SG', 'Singapore');
INSERT INTO `countries` VALUES ('196', 'SK', 'Slovakia');
INSERT INTO `countries` VALUES ('197', 'SI', 'Slovenia');
INSERT INTO `countries` VALUES ('198', 'SB', 'Solomon Islands');
INSERT INTO `countries` VALUES ('199', 'SO', 'Somalia');
INSERT INTO `countries` VALUES ('200', 'ZA', 'South Africa');
INSERT INTO `countries` VALUES ('201', 'GS', 'South Georgia South Sandwich Islands');
INSERT INTO `countries` VALUES ('202', 'ES', 'Spain');
INSERT INTO `countries` VALUES ('203', 'LK', 'Sri Lanka');
INSERT INTO `countries` VALUES ('204', 'SH', 'St. Helena');
INSERT INTO `countries` VALUES ('205', 'PM', 'St. Pierre and Miquelon');
INSERT INTO `countries` VALUES ('206', 'SD', 'Sudan');
INSERT INTO `countries` VALUES ('207', 'SR', 'Suriname');
INSERT INTO `countries` VALUES ('208', 'SJ', 'Svalbard and Jan Mayen Islands');
INSERT INTO `countries` VALUES ('209', 'SZ', 'Swaziland');
INSERT INTO `countries` VALUES ('210', 'SE', 'Sweden');
INSERT INTO `countries` VALUES ('211', 'CH', 'Switzerland');
INSERT INTO `countries` VALUES ('212', 'SY', 'Syrian Arab Republic');
INSERT INTO `countries` VALUES ('213', 'TW', 'Taiwan');
INSERT INTO `countries` VALUES ('214', 'TJ', 'Tajikistan');
INSERT INTO `countries` VALUES ('215', 'TZ', 'Tanzania, United Republic of');
INSERT INTO `countries` VALUES ('216', 'TH', 'Thailand');
INSERT INTO `countries` VALUES ('217', 'TG', 'Togo');
INSERT INTO `countries` VALUES ('218', 'TK', 'Tokelau');
INSERT INTO `countries` VALUES ('219', 'TO', 'Tonga');
INSERT INTO `countries` VALUES ('220', 'TT', 'Trinidad and Tobago');
INSERT INTO `countries` VALUES ('221', 'TN', 'Tunisia');
INSERT INTO `countries` VALUES ('222', 'TR', 'Turkey');
INSERT INTO `countries` VALUES ('223', 'TM', 'Turkmenistan');
INSERT INTO `countries` VALUES ('224', 'TC', 'Turks and Caicos Islands');
INSERT INTO `countries` VALUES ('225', 'TV', 'Tuvalu');
INSERT INTO `countries` VALUES ('226', 'UG', 'Uganda');
INSERT INTO `countries` VALUES ('227', 'UA', 'Ukraine');
INSERT INTO `countries` VALUES ('228', 'AE', 'United Arab Emirates');
INSERT INTO `countries` VALUES ('229', 'GB', 'United Kingdom');
INSERT INTO `countries` VALUES ('230', 'US', 'United States');
INSERT INTO `countries` VALUES ('231', 'UM', 'United States minor outlying islands');
INSERT INTO `countries` VALUES ('232', 'UY', 'Uruguay');
INSERT INTO `countries` VALUES ('233', 'UZ', 'Uzbekistan');
INSERT INTO `countries` VALUES ('234', 'VU', 'Vanuatu');
INSERT INTO `countries` VALUES ('235', 'VA', 'Vatican City State');
INSERT INTO `countries` VALUES ('236', 'VE', 'Venezuela');
INSERT INTO `countries` VALUES ('237', 'VN', 'Vietnam');
INSERT INTO `countries` VALUES ('238', 'VG', 'Virgin Islands (British)');
INSERT INTO `countries` VALUES ('239', 'VI', 'Virgin Islands (U.S.)');
INSERT INTO `countries` VALUES ('240', 'WF', 'Wallis and Futuna Islands');
INSERT INTO `countries` VALUES ('241', 'EH', 'Western Sahara');
INSERT INTO `countries` VALUES ('242', 'YE', 'Yemen');
INSERT INTO `countries` VALUES ('243', 'ZR', 'Zaire');
INSERT INTO `countries` VALUES ('244', 'ZM', 'Zambia');
INSERT INTO `countries` VALUES ('245', 'ZW', 'Zimbabwe');

-- ----------------------------
-- Table structure for interiors
-- ----------------------------
DROP TABLE IF EXISTS `interiors`;
CREATE TABLE `interiors` (
  `id` int(11) NOT NULL,
  `z` double DEFAULT NULL,
  `y` double DEFAULT NULL,
  `x` double DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `interior` int(255) DEFAULT NULL,
  `dimension` int(255) DEFAULT NULL,
  `interior_id` int(11) DEFAULT NULL,
  `on_sale` tinyint(4) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `locked` tinyint(255) DEFAULT '0',
  `disabled` tinyint(255) DEFAULT '0',
  `last_used` int(11) DEFAULT NULL,
  `cost` decimal(10,2) DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of interiors
-- ----------------------------
INSERT INTO `interiors` VALUES ('0', '28.761052', '718.102539', '-2581.498047', 'konto', '0', '0', '80', '0', '14', '0', '0', null, '0.00', '0');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `adminLevel` smallint(6) NOT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `registerDate` datetime NOT NULL,
  `lastLogin` datetime NOT NULL,
  `lastIp` varchar(255) NOT NULL,
  `lastSerial` varchar(255) NOT NULL,
  PRIMARY KEY (`id`,`adminLevel`),
  KEY `adminLevel` (`adminLevel`),
  KEY `id` (`id`),
  CONSTRAINT `adminLevel` FOREIGN KEY (`adminLevel`) REFERENCES `admin_level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'banu', '$2y$10$PkaFRTailNgdtPTxkYVUA.OJmi0B4hAz3mVtr7zTpFXCKba1QWig2', '99', 'banusuryadi@ymail.com', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '3', '4');

-- ----------------------------
-- Table structure for vehicles
-- ----------------------------
DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `on_sale` tinyint(6) DEFAULT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `owner_type` int(11) DEFAULT NULL,
  `model_id` varchar(255) DEFAULT NULL,
  `health` int(100) DEFAULT '100',
  `last_x` double DEFAULT NULL,
  `last_y` double DEFAULT NULL,
  `last_z` double DEFAULT NULL,
  `rot_x` double DEFAULT NULL,
  `rot_y` double DEFAULT NULL,
  `rot_z` double DEFAULT NULL,
  `last_dimension` int(255) DEFAULT NULL,
  `last_interior` int(255) DEFAULT NULL,
  `last_area` varchar(255) DEFAULT NULL,
  `last_used` varchar(11) DEFAULT NULL,
  `date_created` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `lock_state` tinyint(6) DEFAULT '1',
  `light_state` tinyint(4) DEFAULT '1',
  `door_state` varchar(255) DEFAULT NULL,
  `number_plate` varchar(255) DEFAULT NULL,
  `color1` varchar(255) DEFAULT NULL,
  `color2` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user` (`owner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vehicles
-- ----------------------------
INSERT INTO `vehicles` VALUES ('51', '0', '14', '1', '422', '1000', '-2573.66796875', '675.771484375', '27.788732528686523', '0', '0', '244.27099609375', '0', '0', null, null, '1509518831', null, '1', '0', '[ [ 0, 0, 0, 0, 0, 0 ] ]', '123asd', '[ [ 96, 25, 0 ] ]', '[ [ 0 ] ]', '0');
INSERT INTO `vehicles` VALUES ('52', '1', '14', '1', '422', '1000', '-2569.798828125', '672.4326171875', '27.788185119628906', '0', '0', '265.11785888671875', '0', '0', null, null, '1509519451', null, '1', '0', '[ [ 0, 0, 0, 0, 0, 0 ] ]', '123asd', '[ [ 97, 25, 0 ] ]', '[ [ 0 ] ]', '0');
INSERT INTO `vehicles` VALUES ('53', '0', '14', '1', '422', '1000', '-2573.3798828125', '667.2822265625', '27.82366180419922', '0', '0', '182.4005584716797', '0', '0', null, null, '1509519466', null, '1', '0', '[ [ 0, 0, 0, 0, 0, 0 ] ]', '123asd', '[ [ 111, 31, 0 ] ]', '[ [ 0 ] ]', '0');
SET FOREIGN_KEY_CHECKS=1;
