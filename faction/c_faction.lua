local sw, sh = guiGetScreenSize(  )
local gw, gh = 1024, 768
local DGS = exports.dgs
local INT = exports.interior
local faction = {
	window = {},
	label = {},
	gridlist = {},
	tabpanel = {},
	tab = {}
}

local player_factions = {}
local faction_infos = {
	basic_info = {},
	leader = {},
	interior = nil,
	members = {},
	vehicles = {},
}

local function loadFactionList()
	if (not isElement( faction.gridlist[1] )) then return end
	local column = DGS:dgsGridListAddColumn(faction.gridlist[1], "", 1)
	for i,v in ipairs(player_factions) do
		local row = DGS:dgsGridListAddRow(faction.gridlist[1])
		DGS:dgsGridListSetItemText(faction.gridlist[1], row, column, v.faction_name)
		DGS:dgsGridListSetItemData(faction.gridlist[1], row, column, v.faction_id)
	end
end

local function loadFactionInfo( )
	local interiorName = INT:getInteriorNameById(faction_infos.interior)
	faction.label[1] = DGS:dgsCreateLabel(0.05, 0.05, 1, 1, "Faction Name: "..faction_infos.basic_info.name, 1, faction.tab[1])
	faction.label[2] = DGS:dgsCreateLabel(0.05, 0.1, 1, 1, "Leader Name: "..faction_infos.leader.name, 1, faction.tab[1])
	faction.label[3] = DGS:dgsCreateLabel(0.05, 0.15, 1, 1, "Faction Address: "..interiorName, 1, faction.tab[1])

	local columnNumber = DGS:dgsGridListAddColumn(faction.gridlist[2], "No", 0.05)
	local columName = DGS:dgsGridListAddColumn(faction.gridlist[2], "Name", 0.95)
	for i,v in ipairs(faction_infos.members) do
		local row = DGS:dgsGridListAddRow(faction.gridlist[2])
		DGS:dgsGridListSetItemText(faction.gridlist[2], row, columnNumber, tostring(i))
		DGS:dgsGridListSetItemText(faction.gridlist[2], row, columName, v.name)
	end

	local columnNumber2 = DGS:dgsGridListAddColumn(faction.gridlist[3], "No", 0.05)
	local columId = DGS:dgsGridListAddColumn(faction.gridlist[3], "ID", 0.1)
	local columModel = DGS:dgsGridListAddColumn(faction.gridlist[3], "Model", 0.35)
	local columOnSale = DGS:dgsGridListAddColumn(faction.gridlist[3], "On sell?", 0.5)
	for i,v in ipairs(faction_infos.vehicles) do
		local row = DGS:dgsGridListAddRow(faction.gridlist[3])
		DGS:dgsGridListSetItemText(faction.gridlist[3], row, columnNumber2, tostring(i))
		DGS:dgsGridListSetItemText(faction.gridlist[3], row, columModel, getVehicleNameFromModel(v.model_id))
		DGS:dgsGridListSetItemText(faction.gridlist[3], row, columId, tostring(v.id))
		DGS:dgsGridListSetItemText(faction.gridlist[3], row, columOnSale, (v.on_sale == 1 and "Yes" or "No"))
	end
end

local function showFactionGui(  )
	if (isElement( faction.window[1] )) then
		
		for i,v in ipairs(DGS:dgsGetDxGUIFromResource()) do
			if (isElement(v)) then
				destroyElement( v )
			end
		end
		showCursor( false )

		return
	end
	showCursor( true )

	triggerServerEvent( "onClientFactionGUIShow", localPlayer )

	faction.window[1] = DGS:dgsCreateWindow(sw*0.4, sh*0.2, sw*0.25, sh*0.6, "Factions", false, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x96141414, 5)
	faction.gridlist[1] = DGS:dgsCreateGridList(0, 0, 1, 1, true, faction.window[1])

	addEventHandler( "onDgsWindowClose", faction.window[1], function (  )
		showCursor( false )
	end )
end

local function showFactionInfo( factionId, factionName )
	triggerServerEvent( "onClientFactionInfoShow", localPlayer, factionId )

	faction.window[2] = DGS:dgsCreateWindow(sw*0.25, sh*0.2, sw*0.5, sh*0.6, factionName, false, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x96141414, 5, false)
	faction.tabpanel[1] = DGS:dgsCreateTabPanel(0,0,1,1, true, faction.window[2])
	faction.tab[1] = DGS:dgsCreateTab("Basic Info", faction.tabpanel[1])
	faction.tab[2] = DGS:dgsCreateTab("Members", faction.tabpanel[1])
	faction.tab[3] = DGS:dgsCreateTab("Vehicles", faction.tabpanel[1])
	faction.tab[4] = DGS:dgsCreateTab("NPCs", faction.tabpanel[1])

	faction.gridlist[2] = DGS:dgsCreateGridList(0, 0, 1, 1, true, faction.tab[2])
	faction.gridlist[3] = DGS:dgsCreateGridList(0, 0, 1, 1, true, faction.tab[3])
end

local clicked = true
addEventHandler( "onDgsGridListItemDoubleClick", root, function ( button, state, itemId )
	if (not source == faction.gridlist[1]) then return end
	if (clicked) then
		local factionId = DGS:dgsGridListGetItemData(faction.gridlist[1], itemId, 1)
		local factionName = DGS:dgsGridListGetItemText(faction.gridlist[1], itemId, 1)
		showFactionInfo(factionId, factionName)
		clicked = false
	else
		clicked = true
	end
end )
addEvent("onServerLoadFactionInfo", true)
addEventHandler( "onServerLoadFactionInfo", root, function ( faction, leader, members, vehicles, interiorId )
	faction_infos.basic_info = faction
	faction_infos.leader = leader
	faction_infos.members = members
	faction_infos.vehicles = vehicles
	faction_infos.interior = interiorId

	loadFactionInfo()
end )

addEvent("onServerLoadFactions", true)
addEventHandler( "onServerLoadFactions", root, function ( factions )
	player_factions = factions
	loadFactionList()
end )

addEventHandler( "onClientResourceStart", resourceRoot, function (  )
	bindKey("F3", "up", showFactionGui)
end )