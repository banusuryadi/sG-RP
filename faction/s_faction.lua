local DB = exports.database
local CHAR = exports.character
local INT = exports.interior

function createFaction( name, ownerId, interiorId )
	if (name and ownerId and interiorId) then
		local checkChar = CHAR:getPlayerByCharacterId(ownerId)
		local interior = INT:getInteriorById(tonumber(interiorId))
		local checkOwner = DB:query_single("SELECT * FROM factions WHERE `name` = '??'", name)
		if (checkName) or (not isElement(checkChar)) or (not isElement( interior )) then return false end

		local query = DB:insert_id("INSERT INTO factions (`name`, `owner_id`, `is_inactive`, `date_created`, `money`, `interior_id`) VALUES ('??', '??', '??', '??', '??', '??')", name, tonumber(ownerId), 0, getRealTime().timestamp, 0, tonumber(interiorId))
		if (query) then
			setPlayerFaction( ownerId, query )
			INT:setInteriorOwnerType(tonumber(interiorId), 1)
			INT:setInteriorOwner(tonumber(interiorId), query)
			return true, name, ownerId
		end
	end
	return false
end

function getFaction(id)
	if (tonumber(id)) then
		local query = DB:query_single("SELECT * FROM factions WHERE `id` = '??'", tonumber(id))
		if (query) then
			return query
		end
	end
	return false
end

function getPlayerFactions(player)
	local id = getElementData( player, "character.id" )
	if (tonumber(id)) then
		local query = DB:query("SELECT character_faction.*, factions.name as faction_name FROM character_faction LEFT JOIN factions ON character_faction.faction_id = factions.id WHERE character_faction.character_id = '??'", tonumber(id))
		if (query) then
			return query
		end
	end
	return false
end

function getLeaderFaction( id )
	if (tonumber(id)) then
		local query = DB:query_single("SELECT factions.owner_id, characters.name FROM `factions` LEFT JOIN `characters` ON factions.owner_id = characters.id WHERE factions.id = '??'", id)
		if (query) then return query end
	end
	return false
end

function getFactionInteriorId( id )
	if(tonumber(id)) then
		local query = DB:query_single("SELECT interior_id FROM `factions` WHERE `id` = '??'", id)
		if (query) then return query end
	end
	return false
end

function getFactionVehicles( id )
	if(tonumber(id)) then
		local query = DB:query("SELECT * FROM `vehicles` WHERE `owner_type` = 1 AND `owner_id` = '??'", tonumber(id))
		return query or false
	end
	return false
end

function getFactionNPCs( id )
	if(tonumber(id)) then
		local query = DB:query("SELECT * FROM `npc` WHERE `owner_type` = 1 AND `owner_id` = '??'", tonumber(id))
		return query or false
	end
	return false
end

function getFactionMembers( id )
	if(tonumber(id)) then
		local query = DB:query("SELECT character_faction.id, characters.name, characters.last_login, character_faction.join_date FROM `character_faction` LEFT JOIN characters ON character_faction.character_id = characters.id WHERE character_faction.faction_id = '??' AND character_faction.status = 1", tonumber(id))
		return query or false
	end
	return false
end

function setPlayerFaction( playerId, id )
	if (tonumber(playerId)) and (tonumber(id)) then
		local checkPlayerFaction = DB:query_single("SELECT * FROM character_faction WHERE `faction_id` = '??' AND character_id = '??' ", tonumber(id), tonumber(playerId))
		if (checkPlayerFaction) then return false end

		local query = DB:query_single("INSERT INTO character_faction (`character_id`, `faction_id`, `status`, `join_date`) VALUES ('??','??','??','??')", tonumber(playerId), tonumber(id), 1, getRealTime().timestamp)
		if (query) then
			return query
		end
	end
	return false
end

function setFactionOwner(id, ownerId)
	if (tonumber(id)) and (tonumber(ownerId)) then
		local query = DB:query("UPDATE factions SET `owner_id` = '??' WHERE `id` = '??' ", tonumber(ownerId), tonumber(id))
		if (query) then
			return query
		end
	end
	return false
end

function setFactionName(id, name)
	if (tonumber(id)) and (name) then
		local query = DB:query("UPDATE factions SET `name` = '??' WHERE `id` = '??' ", name, tonumber(id))
		if (query) then
			return query
		end
	end
	return false
end

function setFactionActive(id)
	if (tonumber(id)) and (name) then
		local query = DB:query("UPDATE factions SET `is_inactive` = '??' WHERE `id` = '??' ", tonumber(0), tonumber(id))
		if (query) then
			return query
		end
	end
	return false
end

function setFactionInactive(id)
	if (tonumber(id)) and (name) then
		local query = DB:query("UPDATE factions SET `is_inactive` = '??' WHERE `id` = '??' ", tonumber(1), tonumber(id))
		if (query) then
			return query
		end
	end
	return false
end

addEvent("onClientFactionGUIShow", true)
addEventHandler( "onClientFactionGUIShow", root, function (  )
	if(source ~= client) then return end
	local factions = getPlayerFactions(client)
	if (factions) then
		triggerClientEvent( client, "onServerLoadFactions", client, factions )
	end
end )

addEvent("onClientFactionInfoShow", true)
addEventHandler( "onClientFactionInfoShow", root, function ( factionId )
	if(source ~= client) then return end
	local faction = getFaction( factionId )
	local leader = getLeaderFaction( factionId )
	local members = getFactionMembers( factionId )
	local vehicles = getFactionVehicles( factionId )
	local interior = getFactionInteriorId( factionId )
	if(leader and members) then
		triggerClientEvent( client, "onServerLoadFactionInfo", client, faction, leader, members, vehicles, interior.interior_id )
	end

end )
-- commands

local addCommandHandler_ = addCommandHandler
	addCommandHandler = function(commandName, fn, restricted, caseSensitive)
	if (type(commandName) ~= "table") then
		commandName = {commandName}
	end
	for key, value in ipairs(commandName) do
		if (key == 1) then
			addCommandHandler_(value, fn, restricted, false)
		else
			addCommandHandler_(value,
				function(player, ...)
					fn(player, ...)
				end, false, false
			)
		end
	end
end

addCommandHandler( {"createfact", "cfact", "makefact", "makefaction", "createfaction", "addfact", "addfaction"},
	function ( commander, cmdName, ownerId, interiorId, ...)
		local admin_level = getElementData( commander, "user.adminLevel" )
		if (admin_level) then
			if (admin_level > 4) then
				local name = table.concat({...}, " ")
				if (name and ownerId and interiorId) then
					if (createFaction(name, ownerId, interiorId)) then
						outputChatBox( "Faction "..name.." berhasil dibuat." )
					end
				else
					outputChatBox( "/"..cmdName.." [leader id] [interior id] [nama faction]", commander, 3,169,244 )
				end
			end
		end
	end 
)