function getInteriorId( interior )
	return ( isElement( interior ) and getElementData( interior, "interior.id" ) ) and tonumber( getElementData( interior, "interior.id" ) ) or false
end

function getInteriorById( interiorId )
	for _, interior in ipairs( getElementsByType( "marker" ) ) do
		if ( getInteriorId( interior ) == interiorId ) then
			return interior
		end
	end	
	return false
end

function getInteriorNameById( interiorId )
	local int = getInteriorById( interiorId )
	return getElementData(int, "interior.name") or false
end