local sw, sh = guiGetScreenSize(  )
local gw, gh = 1024, 768

local DGS = exports.dgs
local int = {
	window = {},
	label = {},
	button = {}
}

local function showInteriorSaleGUI( interior )
	if (isElement( int.window[1] )) then
		for i,v in ipairs(DGS:dgsGetDxGUIFromResource()) do
			if (isElement( v )) then
				destroyElement( v )
			end
		end
		showCursor( false )

		addEventHandler( "onDgsMouseClick", int.button[1], function (  )
			triggerServerEvent( "onPlayerBuyInterior", localPlayer, interior )
			showInteriorSaleGUI()
		end )

		removeEventHandler( "onDgsMouseClick", int.button[2], function (  )
			showInteriorSaleGUI()
		end )

		return
	end
	showCursor( true )

	local price = getElementData(interior, "interior.cost")
	local intName = getElementData(interior, "interior.name")

	int.window[1] = DGS:dgsCreateWindow(sw*0.35, sh*0.3, sw*0.3, sh*0.2, "Buy Property", false, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x96141414, 5, true)

	int.label[1] = DGS:dgsCreateLabel(0.05, 0.05, 1, 1, intName, true, int.window[1], nil, 1.2, 1.2)
	int.label[2] = DGS:dgsCreateLabel(0.05, 0.25, 1, 1, "$ "..price, true, int.window[1])

	int.button[1] = DGS:dgsCreateButton(0.05, 0.75, 0.3, 0.2, "Buy", 1, int.window[1])
	int.button[2] = DGS:dgsCreateButton(0.65, 0.75, 0.3, 0.2, "Close", 1, int.window[1])

	addEventHandler( "onDgsMouseClick", int.button[1], function (  )
		triggerServerEvent( "onPlayerBuyInterior", localPlayer, interior )
		showInteriorSaleGUI()
	end )

	addEventHandler( "onDgsMouseClick", int.button[2], function (  )
		showInteriorSaleGUI()
	end )
end

addEvent("onEnterSaleInterior", true)
addEventHandler( "onEnterSaleInterior", root, function ( interior )
	showInteriorSaleGUI( interior )
end )