local interiors = {
	-- Houses
	[1]  = { 235.25,	 1186.68,	1080.26,	3},  -- House 1
	[2]  = { 226.79,	 1240.02,	1082.14,	2},  -- House 2
	[3]  = { 223.07,	 1287.09,	1082.14,	1},  -- House 3
	[4]  = { 327.94,	 1477.73,	1084.44,	15}, -- House 4
	[5]  = { 2468.84,	-1698.29,	1013.51,	2},  -- House 5
	[6]  = { 226.34,	 1114.23,	1080.89,	5},  -- House 6
	[7]  = { 387.23,	 1471.79,	1080.19,	15}, -- House 7
	[8]  = { 225.79,	 1021.46,	1084.02,	7},  -- House 8
	[9]  = { 295.16,	 1472.26,	1080.26,	15}, -- House 9
	[10] = { 2807.58,	-1174.75,	1025.57,	8},  -- House 10
	[11] = { 2270.42,	-1210.52,	1047.56,	10}, -- House 11
	[12] = { 2496.02,	-1692.08,	1014.74,	3},  -- House 12
	[13] = { 2259.38,	-1135.84,	1050.64,	10}, -- Motel Room 1
	[14] = { 2365.21,	-1135.60,	1050.88,	8},  -- House 14
	[15] = { 1531.36,	-6.84,		1002.01,	3},  -- Room 5
	[17] = { 2233.8,	-1115.36,	1050.89,	5},  -- Hotel Room 1
	[18] = { 2282.90,	-1140.27,	1050.9,		11}, -- House 18
	[19] = { 2196.75,	-1204.34,	1048.84,	6},  -- House 19
	[20] = { 2308.78,	-1212.91,	1048.82,	6},  -- House 20
	[21] = { 2218.14,	-1076.24, 	1050.48,	1},  -- Hotel Room 2
	[22] = { 2237.61,	-1081.48,	1048.91,	2},  -- House 22
	[23] = { 2317.83, 	-1026.46, 	1050.21,	9},  -- House 23
	[24] = { 260.98,	 1284.40,	1080.08,	4},  -- House 24
	[25] = { 140.29, 	 1365.99, 	1083.85,	5},  -- House 25
	[26] = { 83.02, 	 1322.51, 	1083.86,	9},  -- House 26
	[27] = {-42.61, 	 1405.71, 	1084.42,	8},  -- House 27
	[28] = { 2333.09,	-1077.06, 	1049.02,	6},  -- House 28
	[29] = { 1298.95,	-797.01,	1084.01,	5},  -- Madd Dogg's
	[30] = { 243.71,	 304.95,	999.14,		1},  -- Room 1
	[31] = { 266.50,	 305.01,	999.14,		2},  -- Room 2
	[32] = { 322.18,	 302.35,	999.14,		5},  -- Room 3
	[33] = { 343.71,	 304.98,	999.14,		6},  -- Room 4
	[34] = {-2102.16, 	 896.37, 	76.7,		0},  -- Small Garage 1
	[35] = { 2648.16, 	-2039.65, 	13.56,		0},  -- Small Garage 2
	-- Businesses
	[36] = {-25.89,		-188.24,	1003.54,	17}, -- 24/7 1
	[37] = { 6.11,		-31.75,		1003.54,	10}, -- 24/7 2
	[38] = {-25.89,		-188.24,	1003.54,	17}, -- 24/7 3
	[39] = {-25.77,		-141.55,	1003.55,	16}, -- 24/7 4
	[40] = {-27.30,		-31.76,		1003.56,	4},  -- 24/7 5
	[41] = {-27.34,		-58.26,		1003.55,	6},  -- 24/7 6
	[42] = { 285.50,	-41.80,		1001.52,	1},  -- Ammunation 1
	[43] = { 285.87,	-86.78,		1001.52,	4},	 -- Ammunation 2
	[44] = { 296.84,	-112.06,	1001.52,	6},  -- Ammunation 3
	[45] = { 315.70,	-143.66,	999.60,		7},  -- Ammunation 4
	[46] = { 316.32,	-170.30,	999.60,		6},  -- Ammunation 5
	[47] = { 1727.04,	-1637.84,	20.22,		18}, -- Atrium
	[48] = { 501.99,	-67.56,		998.75,		11}, -- Bar 1
	[49] = {-229.3,		 1401.28,	27.76,		18}, -- Bar 2
	[50] = { 1212.12,	-26.14,		1000.99,	3},  -- Bar 3
	[51] = { 681.58,	-450.89,	-25.37,		1},  -- Bar 4
	[52] = { 362.84,	-75.13,		1001.50,	10}, -- Burgershot
	[53] = { 207.63,	-111.26,	1005.13,	15}, -- Clothes Shop 1
	[54] = { 204.32,	-168.85,	1000.52,	14}, -- Clothes Shop 2
	[55] = { 207.07,	-140.37,	1003.51,	3},  -- Clothes Shop 3
	[56] = { 203.81,	-50.66,		1001.80,	1},  -- Clothes Shop 4
	[57] = { 227.56,	-8.06,		1002.21,	5},  -- Clothes Shop 5
	[58] = { 161.37,	-97.11,		1001.80,	18}, -- Clothes Shop 6
	[59] = { 493.50,	-24.95,		1000.67,	17}, -- Club 1
	[60] = {-2636.66,	 1402.36,	906.50,		3},  -- Club 2
	[61] = { 364.98,	-11.84,		1001.85,	9},  -- Cluckin' Bell
	[62] = { 460.53,	-88.62,		999.55,		4},  -- Diner 1
	[63] = { 441.90,	-49.70,		999.74,		6},  -- Diner 2
	[64] = { 377.08,	-193.30,	1000.63,	17}, -- Donut Shop
	[65] = {-2240.77,	 137.20,	1035.41,	6},  -- Electronics Shop
	[66] = { 964.93,	 2160.09,	1011.03,	1},  -- Slaughterhouse
	[67] = { 390.76,	 173.79,	1008.38,	3},  -- Office 1
	[68] = {-2026.86,	-103.60,	1035.18,	3},  -- Office 2
	[69] = { 1494.36,	 1303.57,	1093.28,	3},  -- office 3
	[70] = { 372.33,	-133.52,	1001.49,	5},  -- The Well Stacked Pizza Co.
	[71] = {-100.34,	-25.03,		1000.72,	3},  -- Sex Shop
	[72] = { 412,		-23,		1002,		2},  -- Reece's Barber Shop
	[73] = { 418.6,		-84.17,		1001.70,	3},  -- Barber Shop
	[74] = {-204.37,	-8.90,		1002.26,	17}, -- Tattoo Shop
	[75] = { 2541.71,	-1304.07,	1025.08,	2},  -- Factory
	[76] = {-977.72,	 1052.96,	1345.22,	10}, -- RC Battlefield
	[77] = { 2266.15,	 1647.42,	1084.29,	1},  -- Casino Hallway
	[78] = { 834.78,	 7.42,		1003.97,	3},  -- Betting Shop 1
	[79] = {-2158.58, 	 643.15,	1052.33,	1},  -- Betting Shop 2
	[80] = { 2214.38, 	-1150.48, 	1025.79,	15}, -- Jefferson Motel
	[81] = { 773.89,	-78.85, 	1000.66,	7},  -- Gym 1
	[82] = { 772.34, 	-5.52, 		1000.72,	5},  -- Gym 2
	[83] = { 774.18,	-50.42,		1000.60,	6},  -- Gym 3
	[85] = {-1426.14,	 928.44,	1036.35,	15}, -- Stadium 1
	[86] = {-1426.13,	 44.16,		1036.23,	1},  -- Stadium 2
	[87] = {-1464.72,	 1555.93,	1052.68,	14}, -- Stadium 3
	[89] = { 1420.15, 	 6.71, 		1002.39,	1},  -- Warehouse 2
	-- Government
	[90] = { 246.75,	 62.32,		1003.64,	6},  -- Los Santos Police Department
	[91] = { 246.48,	 107.30,	1003.22,	10}, -- San Fierro Police Department
	[92] = { 238.72,	 138.62,	1003.02,	3},	 -- Las Venturas Police Department
}

local DB = exports.database

local function addNewInterior( name, x, y, z, interior, dimension, interiorId, ownerId, ownerType, onSale, cost )
	local query = DB:execute("INSERT INTO interiors (`name`, `interior`, `dimension`,`interior_id`, `x`, `y`, `z`, `on_sale`, `owner_id`, `ownerType`, `cost` ) VALUES ('??', '??', '??', '??', '??', '??', '??', '??', '??', '??' )", name, interior, dimension, interiorId, x, y, z, onSale, ownerId, ownerType, cost )
	if (query) then
		return true
	end
	return false
end

local function addInterior( id, name, x, y, z, interior, dimension, lastUsed, locked, disabled, interiorId, ownerId, ownerType, onSale, cost )
	local eMarker = createMarker( x, y, z, "arrow", 1.5, 255, disabled == 0 and 255 or 0, 0, 255 )
	setElementInterior(eMarker, interior)
	setElementDimension(eMarker, dimension)
	setElementData( eMarker, "interior.id", id )
	setElementData( eMarker, "interior.name", name )
	setElementData( eMarker, "interior.lastUsed", lastUsed )
	setElementData( eMarker, "interior.locked", locked )
	setElementData( eMarker, "interior.disabled", disabled )
	setElementData( eMarker, "interior.interiorId", interiorId )
	setElementData( eMarker, "interior.ownerId", ownerId )
	setElementData( eMarker, "interior.ownerType", ownerType )
	setElementData( eMarker, "interior.onSale", onSale )
	setElementData( eMarker, "interior.cost", cost )
	setElementData( eMarker, "interior.way", 0 )

	local sMarker = createMarker( interiors[interiorId][1], interiors[interiorId][2], interiors[interiorId][3]+0.8, "arrow", 1.5, 255, disabled == 0 and 255 or 0, 0, 255 )
	setElementInterior(sMarker, interiors[interiorId][4])
	setElementDimension(sMarker, id)
	setElementData( sMarker, "interior.id", id )
	setElementData( sMarker, "interior.name", name )
	setElementData( sMarker, "interior.lastUsed", lastUsed )
	setElementData( sMarker, "interior.locked", locked )
	setElementData( sMarker, "interior.disabled", disabled )
	setElementData( sMarker, "interior.interiorId", interiorId )
	setElementData( sMarker, "interior.ownerId", ownerId )
	setElementData( sMarker, "interior.ownerType", ownerType )
	setElementData( sMarker, "interior.onSale", onSale )
	setElementData( sMarker, "interior.cost", cost )
	setElementData( sMarker, "interior.way", 1 )
	setElementParent( sMarker, eMarker )
end

local function loadInteriors()
	local result, num_affected_rows, last_insert_id = DB:query("SELECT * FROM interiors")
	if (result) then
		if (num_affected_rows > 0) then
			for _,row in pairs(result) do
				if (row["is_deleted"] == 0) then
					addInterior(row["id"], row["name"], row["x"], row["y"], row["z"], row["interior"], row["dimension"], row["last_used"],  row["locked"], row["disabled"], row["interior_id"], row["owner_id"], row["owner_type"], row["on_sale"], row["cost"])
				end
			end
		end
	end
end

local hitMarker = {}
local function enterInterior( player )
	if (not isElement( hitMarker[player] )) then return end
	local onSale = getElementData(hitMarker[player], "interior.onSale")
	if (tonumber(onSale) == 1) then
		triggerClientEvent( player, "onEnterSaleInterior", player, hitMarker[player] )
		return
	end
	if (getElementData(hitMarker[player], "interior.locked") == 1) then 
		outputChatBox( "Door locked.", player, 231, 0, 0 )
		return 
	end
	local way = getElementData( hitMarker[player], "interior.way" )
	if (way == 0) then
		local child = getElementChild( hitMarker[player], 0 )
		if(isElement( child )) then
			local x, y, z = getElementPosition( child )
			local dimension = getElementDimension( child )
			local interior = getElementInterior( child )
			setElementFrozen( player, true )
			setTimer(setElementFrozen, 300, 1, player, false)
			setElementPosition( player, x, y, z-0.3 )
			setElementInterior( player, interior )
			setElementDimension( player, dimension )

		end
	else
		local parent = getElementParent( hitMarker[player] )
		if(isElement( parent )) then
			local x, y, z = getElementPosition( parent )
			local dimension = getElementDimension( parent )
			local interior = getElementInterior( parent )
			setElementFrozen( player, true )
			setTimer(setElementFrozen, 300, 1, player, false)
			setElementPosition( player, x, y, z-0.3 )
			setElementInterior( player, interior )
			setElementDimension( player, dimension )
		end
	end
end
local INVENTORY = exports.inventory
local function lockInterior( player )
	if (not isElement( hitMarker[player] )) then return end
	local interiorId = getElementData(hitMarker[player], "interior.id")
	if (INVENTORY:hasItem(player, 2, tostring(interiorId))) then
		local lockState = getElementData(hitMarker[player], "interior.locked")
		setInteriorLocked(interiorId, (lockState == 0 and 1 or 0))
		outputChatBox( "Door ".. (getElementData(hitMarker[player], "interior.locked") == 1 and "locked." or "unlocked."), player)
	end
end

function getInteriorById( interiorId )
	for i,v in ipairs(getElementsByType( "marker" )) do
		if (isElement( v )) then
			if(getElementData(v, "interior.id") == interiorId) then
				return v
			end
		end
	end
end

function setInteriorOwner( interiorId, ownerId )
	for i,v in ipairs(getElementsByType( "marker" )) do
		if (isElement( v )) then
			if(getElementData(v, "interior.id") == interiorId) then
				setElementData( v, "interior.ownerId", tonumber(ownerId) )
			end
		end
	end
end


function setInteriorOwnerType( interiorId, ownerType )
	for i,v in ipairs(getElementsByType( "marker" )) do
		if (isElement( v )) then
			if(getElementData(v, "interior.id") == interiorId) then
				setElementData( v, "interior.ownerType", tonumber(ownerType) )
			end
		end
	end
end

function setInteriorOnSale( interiorId, status )
	for i,v in ipairs(getElementsByType( "marker" )) do
		if (isElement( v )) then
			if(getElementData(v, "interior.id") == interiorId) then
				setElementData( v, "interior.onSale", status )
			end
		end
	end
end

function setInteriorLocked( interiorId, status )
	for i,v in ipairs(getElementsByType( "marker" )) do
		if (isElement( v )) then
			if(getElementData(v, "interior.id") == interiorId) then
				setElementData( v, "interior.locked", tonumber(status) )
			end
		end
	end
end

function setInteriorPrice( interiorId, price )
	for i,v in ipairs(getElementsByType( "marker" )) do
		if (isElement( v )) then
			if(getElementData(v, "interior.id") == interiorId) then
				setElementData( v, "interior.cost", tonumber(price) )
			end
		end
	end
end
function deleteInterior( player )
	if (not isElement( hitMarker[player] )) then return end
	local way = getElementData( hitMarker[player], "interior.way" )
	local id = getElementData( hitMarker[player], "interior.id" )
	local query = DB:execute("UPDATE interiors SET `is_deleted` = '??' WHERE `id` = '??'", 1, id)

	if(query) then
		if (way == 0) then
			destroyElement( hitMarker[player] )
		else
			local parent = getElementParent( hitMarker[player] )
			if(isElement( parent )) then
				destroyElement( parent )
			end
		end
	else
		outputChatBox( "Kesalahan ketika menghapus interior, hubungi scipter.", player )
	end
end

addEventHandler( "onPlayerMarkerHit", root, function ( marker )
	if (getElementData(marker, "interior.id")) then
		hitMarker[source] = marker
	end
end )

addEventHandler( "onPlayerMarkerLeave", root, function ( marker )
	if (hitMarker[source] == marker) then
		hitMarker[source] = nil
	end
end )

addEventHandler( "onResourceStart", resourceRoot, function ( )
	local players = getElementsByType( "player" )
	for _,player in pairs(players) do
		bindKey(player, "f", "up", enterInterior)
		bindKey(player, "k", "up", lockInterior)
	end
	loadInteriors()
	outputServerLog( "Messages: Don't forget to restart faction resource!" )
end )

addEventHandler( "onPlayerSpawn", root, function ( )
	bindKey(source, "f", "up", enterInterior)
	bindKey(source, "k", "up", lockInterior)
end )
addEventHandler( "onResourceStop", resourceRoot, function ( )
	for i,v in ipairs(getElementsByType( "marker" )) do
		if (isElement( v )) then
			local id = getElementData(v, "interior.id")
			local isEnterGate = getElementChild( v, 0 )
			if (id) and (isElement(isEnterGate)) then
				local x, y, z = getElementPosition( v )
				local query = DB:query("UPDATE interiors SET `x` = '??', `y` = '??',`z` = '??',`name` = '??',`interior` = '??',`dimension` = '??',`interior_id` = '??',`on_sale` = '??',`owner_id` = '??',`locked` = '??',`disabled` = '??',`cost` = '??', `owner_type` = '??' WHERE `id` = '??'",x,y,z,getElementData(v, "interior.name"), getElementInterior( v ), getElementDimension( v ), getElementData(v, "interior.interiorId"), getElementData(v, "interior.onSale"), getElementData(v, "interior.ownerId"), getElementData( v, "interior.locked" ), getElementData(v, "interior.disabled"), getElementData(v, "interior.cost"), getElementData(v, "interior.ownerType"), tonumber(id))
				if (query) then
					outputServerLog( "INTERIOR: Berhasil mengupdate interior ke dalam database. ID: "..id )
				end
			end
		end
	end
end )

-- commands
local addCommandHandler_ = addCommandHandler
	addCommandHandler  = function(commandName, fn, restricted, caseSensitive)
	if (type(commandName) ~= "table") then
		commandName = {commandName}
	end
	for key, value in ipairs(commandName) do
		if (key == 1) then
			addCommandHandler_(value, fn, restricted, false)
		else
			addCommandHandler_(value,
				function(player, ...)
					fn(player, ...)
				end, false, false
			)
		end
	end
end

addCommandHandler( {"addint","addinterior","createinterior","createint"}, function ( player, cmd, name, intId, ownerId, onSale, cost )
	if (name and intId and ownerId and onSale and cost) then
		local admin_level = getElementData( player, "user.adminLevel" )
		if (admin_level) then
			if (admin_level > 4) then
				local x, y, z = getElementPosition( player )
				local interior = getElementInterior( player )
				local dimension = getElementDimension( player )
				addNewInterior(name, x ,y, z+0.8, interior, dimension, intId, ownerId, 0, onSale, cost)
				loadInteriors()
			end
		end
	else
		outputChatBox( "/"..cmd.." [nama interior] [interior id] [player id] [on sale] [harga]", player, 3,169,244 )
	end
end )

addCommandHandler( {"delint","deleteinterior","delinterior","deleteint"}, function ( player, cmd )
	local admin_level = getElementData( player, "user.adminLevel" )
	if (admin_level) then
		if (admin_level > 4) then
			deleteInterior(player)
			loadInteriors()
		end
	end
end )