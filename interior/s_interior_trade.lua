addEvent( "onPlayerBuyInterior", true )
addEventHandler( "onPlayerBuyInterior", root, function ( interior )
	if (source ~= client) then return end
	local ownerId = getElementData(client, "character.id")
	local interiorId = getElementData(interior, "interior.id")
	setInteriorOwnerType(interiorId, 0)
	setInteriorOwner(interiorId, ownerId)
	setInteriorOnSale(interiorId, 0)
end )