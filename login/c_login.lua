local sw, sh = guiGetScreenSize(  )
local gw, gh = 1024, 768
local login = {
	window = {},
	edit = {},
	button = {},
	label = {}
}
local DGS = exports.dgs

local function setTooltip( text, r, g, b, a )
	DGS:dgsSetText(login.label[4], text)
	DGS:dgsLabelSetColor(login.label[4], r, g, b, a)
end


local function processLogin(  )
	DGS:dgsSetEnabled(login.button[1], false)

	setTooltip("Mencoba login...", 0, 255, 0, 255)

	local username = DGS:dgsGetText(login.edit[1])
	local password = DGS:dgsGetText(login.edit[2])
	if (not username or #username > 3) then
		if (not password or #password > 4) then
			triggerServerEvent( "playerLoginCheck", localPlayer, username, password )
		else
			setTooltip("ERROR: Password terlalu pendek.", 255, 0, 0, 255)
		end
	else
		setTooltip("ERROR: Username terlalu pendek.", 255, 0, 0, 255)
	end
end

local function displayLogin( forceStop )
	if ( forceStop ) then
		for _,v in ipairs(DGS:dgsGetDxGUIFromResource()) do
			if (isElement( v )) then
				-- DGS:dgsSetVisible(v, false)
				destroyElement( v )
			end
		end
		showCursor( false )

		return
	end
	showCursor( true )
	DGS:setSystemFont('OpenSans.ttf', 12)
	login.window[1] = DGS:dgsCreateWindow(sw*0, sh*0.25, sw*1, sh*0.4, "", false, 0xFFFFFFFF, 25, nil, 0xC8141414, nil, 0x96141414, 5, true)

	DGS:dgsWindowSetSizable(login.window[1], false)
	DGS:dgsWindowSetMovable(login.window[1], false)
	login.label[1] = DGS:dgsCreateLabel( 0.35, 0.20, 0.1, 0.05, "Sign In to Play.", true, login.window[1] )
	login.edit[1] = DGS:dgsCreateEdit( 0.35, 0.30, 0.2, 0.1, "", true, login.window[1] )
	login.label[2] = DGS:dgsCreateLabel( 0.35, 0.40, 0.1, 0.1, "Username", true, login.window[1] )

	login.edit[2] = DGS:dgsCreateEdit( 0.35, 0.50, 0.2, 0.1, "", true, login.window[1] )
	DGS:dgsSetProperty(login.edit[2],"masked",true)
	login.label[3] = DGS:dgsCreateLabel( 0.35, 0.60, 0.1, 0.1, "Password", true, login.window[1] )

	login.button[1] = DGS:dgsCreateButton( 0.35, 0.70, 0.09, 0.1, "Enter", true, login.window[1] )
	login.button[2] = DGS:dgsCreateButton( 0.45, 0.70, 0.09, 0.1, "Register", true, login.window[1] )
	login.label[4] = DGS:dgsCreateLabel( 0.35, 0.81, 0.1, 0.05, "", true, login.window[1] )

	
	-- addEventHandler( "onClientRender", root, function ( )
	-- 	dxDrawRectangle( sw*0, sh*0.25, sw*1, sh*0.4,  tocolor ( 0, 0, 0, 150 ) )
	-- end )

	-- addEventHandler ( "onDgsMouseClick", login.button[2], function ( button, state )
	-- 	displayLogin(true)
	-- end)

	addEventHandler ( "onDgsMouseClick", login.button[1], function ( button, state )
		if (state == "up") then
			processLogin()
		end
	end )

	addEventHandler( "onClientKey", root, function ( button, press )
		if ( button == "enter") and (not press) then
			processLogin()
		end
	end )

	addEventHandler( "onDgsFocus", login.edit[1], function (  )
		setTooltip("")
	end )

	addEventHandler( "onDgsFocus", login.edit[2], function (  )
		setTooltip("")
	end )

end

local function initLogin( )
	setPlayerHudComponentVisible( "all", false )
	startView()
	showCursor( true )
end
-- addEventHandler( "onClientResourceStart", getResourceRootElement(), initLogin )

addEvent( "clientInitLogin", true )
addEventHandler( "clientInitLogin", root, initLogin )

addEvent( "clientDisplayLogin", true )
addEventHandler( "clientDisplayLogin", root, displayLogin )

addEvent( "clientRetrieveLogin", true )
addEventHandler( "clientRetrieveLogin", root, function (isSuccess)
	if (isSuccess) then
		setTooltip("Login berhasil.", 0, 255, 0, 255)
		setTooltip("Mulai masuk dengan user.", 0, 255, 0, 255)
		displayLogin( true )
	else
		setTooltip("Login gagal.", 255, 0, 0, 255)
		DGS:dgsSetEnabled(login.button[1], true)
	end
end )

addEvent( "onServerQueryLogin", true )
addEventHandler( "onServerQueryLogin", root, function (msg, status)
	if (msg) then
		if (status == true) then
			setTooltip(msg, 0, 255, 0, 255)
		else
			setTooltip(msg, 255, 0, 0, 255)
			DGS:dgsSetEnabled(login.button[1], true)
		end
	end
end )