local database = exports.database

local function setPlayerData(data, player)
	setElementData( player, "user.id", tonumber(data.id), true )
	setElementData( player, "user.username", data.username, true )
	setElementData( player, "user.adminLevel", data.adminLevel, true )
	setElementData( player, "user.email", data.email, true )
	setElementData( player, "user.registerDate", data.registerDate, true )
	setElementData( player, "user.lastLogin", data.lastLogin, true )
	setElementData( player, "user.lastIp", data.lastIp, true )
	setElementData( player, "user.isLoggedIn", tonumber(1), true )
	setElementData( player, "user.state", tonumber(2), true )
end

addEvent( "playerLoginCheck", true )
addEventHandler( "playerLoginCheck", root, function ( username, password )
	if (source ~= client) then
		return
	end

	local user = database:query_single("SELECT * FROM `??` WHERE `??` = '??' LIMIT 1", "users", "username", username)
	triggerClientEvent( client, "onServerQueryLogin", client, "Verifikasi username...", true)

	if (user) then
		triggerClientEvent( client, "onServerQueryLogin", client, "Verifikasi password...", true)
		if (passwordVerify(password, user.password)) then
			setPlayerData(user, client)
			triggerClientEvent( client, "clientRetrieveLogin", client, true )
			-- triggerEvent( "onInitCharacter", client )
		else
			triggerClientEvent( client, "clientRetrieveLogin", client, false )
		end
	else
		triggerClientEvent( client, "onServerQueryLogin", client, "Terjadi masalah pada server. Hubungi admin.", false)
	end
end )

addEventHandler( "onElementDataChange", root, function ( dataName )
	if (getElementType(source) == "player") and (dataName == "user.state") then
		if (getElementData( source, "user.state" ) == 1) then				
			triggerClientEvent( source, "clientInitLogin", resourceRoot )
			triggerClientEvent( source, "clientDisplayLogin", resourceRoot )
		end
	end
end )

addEventHandler( "onPlayerJoin", root, function ()
	outputChatBox( "~~Please /reconnect if blank screen occured.~~", source, 231, 0, 0 )
	if (getElementType(source) == "player") then
		setElementData( source, "user.state", 1, true )
	end
end )

-- addEventHandler( "onResourceStart", root, function (  )
-- 	for _,player in ipairs(getElementsByType( "player" )) do
-- 		if (getElementData(player, "user.state" ) == 1 ) then
-- 			triggerClientEvent( player, "clientDisplayLogin", resourceRoot )
-- 		end
-- 	end
-- end )